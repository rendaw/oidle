extern crate common;
extern crate rpassword;
extern crate sodiumoxide;
extern crate structopt;

use common::bindings_fido2::COSE_ES256;
use common::fido2::{Fido2Cred, Fido2Dev, Fido2DevList};
use common::{b64e, rand_str, sha256};
use common::{rand, serde_json};
use sodiumoxide::crypto::pwhash::{MEMLIMIT_INTERACTIVE, OPSLIMIT_INTERACTIVE};
use std::collections::HashMap;
use std::ffi::{CStr, CString};
use std::io::{stdout, Write};
use std::process::{exit, Command, Stdio};
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(about)]
struct Args {
    /// The domain this will be hosted on.  This is really only used when generating the WebAuthn key.  See PublicKeyCredentialRequestOptions.rpId in the WebAuthn/ECMAScript specs.
    domain: String,

    /// The username to use to log in
    username: String,

    /// Enable passwords; you will be prompted for your password interactively.  You must enable at least either a password or WebAuthn.
    #[structopt(short, long)]
    password: bool,

    /// Enable WebAuthn in the config and interactively generate a WebAuthn key.  Specify multiple times to set up multiple authenticators.  Authenticators will be automatically enumerated, so detach any authenticators you don't want to use.
    #[structopt(short, long, parse(from_occurrences))]
    webauthn: u8,

    /// Specify a WebAuthn authenticator device node, enable WebAuthn in the config, and interactively generate a WebAuthn key.  Specify multiple times to set up multiple authenticators.
    #[structopt(short = "d", long)]
    webauthn_device: Vec<String>,
}

fn die(text: &str) {
    writeln!(std::io::stderr(), "{}", text).unwrap();
    exit(1);
}

fn main() {
    let args: Args = Args::from_args();

    let username = args.username;

    let domain = args.domain;

    if !args.password && args.webauthn == 0 {
        die("You must at least enable password or WebAuthn.")
    }

    let password = (if args.password {
        let pw = rpassword::prompt_password_stderr("Enter your password: ").unwrap();
        let confirm =
            rpassword::prompt_password_stderr("Enter your password again to confirm: ").unwrap();
        if pw.ne(&confirm) {
            die("Passwords don't match!");
        }
        Some(pw)
    } else {
        None
    })
    .map(|v| {
        let temp = sodiumoxide::crypto::pwhash::pwhash(
            v.as_bytes(),
            OPSLIMIT_INTERACTIVE,
            MEMLIMIT_INTERACTIVE,
        )
        .unwrap();
        temp.0[0..temp.0.len()].to_vec()
    });

    fn do_webauthn(domain: &str, path: &CStr) -> (String, String) {
        writeln!(
            std::io::stderr(),
            "Complete confirmation on the WebAuthn device.",
        )
        .unwrap();
        let dev = Fido2Dev::new(path).unwrap();

        let cred = Fido2Cred::new().unwrap();
        cred.set_type(COSE_ES256).unwrap();
        cred.set_clientdata_hash(&sha256(&[0u8; 1])).unwrap();
        cred.set_rp(&domain, "").unwrap();

        dev.make_cred(&cred, None).unwrap();

        let webauthn_id = b64e(&cred.id().unwrap().unwrap());
        let webauthn_key = b64e(&cred.pubkey().unwrap().unwrap());
        (webauthn_id, webauthn_key)
    }
    let mut webauthn = Vec::new();
    for path in Fido2DevList::new(1)
        .unwrap()
        .into_iter()
        .take(args.webauthn as usize)
    {
        webauthn.push(do_webauthn(&domain, &path));
    }
    for path in args.webauthn_device {
        webauthn.push(do_webauthn(
            &domain,
            &CString::new(path.as_bytes()).unwrap(),
        ));
    }

    let pubpriv_key;
    {
        let pre_priv_key = Command::new("openssl")
            .args(&["genrsa"])
            .stderr(Stdio::inherit())
            .stdin(Stdio::null())
            .output()
            .unwrap()
            .stdout;
        let mut priv_command = Command::new("openssl")
            .args(&["rsa", "-outform", "DER"])
            .stderr(Stdio::inherit())
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()
            .unwrap();
        let mut stdin = priv_command.stdin.take().unwrap();
        stdin.write(pre_priv_key.as_slice()).unwrap();
        drop(stdin);
        pubpriv_key = priv_command.wait_with_output().unwrap().stdout;
    }

    serde_json::to_writer_pretty(
        stdout(),
        &common::Configuration {
            username,
            jwt_pubpriv_key_der: pubpriv_key,
            general_key: sodiumoxide::randombytes::randombytes(
                sodiumoxide::crypto::sign::SEEDBYTES,
            )
            .to_vec(),
            general_key_mask: sodiumoxide::randombytes::randombytes(
                sodiumoxide::crypto::sign::SEEDBYTES,
            )
            .to_vec(),
            password: password.map(|v| v.into()),
            domain,
            webauthn,
            confirm: true,
            listen: None,
            listen_ldap: None,
            authentication_token_expiry: None,
            access_token_expiry: None,
            id_token_verification_expiry: None,
            clients: HashMap::new(),
            force_https: true,
            identities: vec![common::ConfigurationIdentity {
                name: "default".to_string(),
                pairwise_subject: true,
                sub: rand_str(&mut rand::thread_rng(), 30),
                claims: HashMap::new(),
            }],
            session_incomplete_expiry: None,
            session_valid_expiry: None,
            ldap_attributes: HashMap::new(),
        },
    )
    .unwrap();
}
