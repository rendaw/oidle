extern crate chrono;
extern crate common as common_shared;
extern crate http;
extern crate hyper;
extern crate jsonwebtoken;
extern crate regex;
extern crate ring;
extern crate serde_with_macros;
extern crate time;

use std::str::FromStr;

use crate::oidc::common::JWTPublicKey;
use chrono::{DateTime, Utc};
pub use common_shared::bindings_fido2::{COSE_ES256, FIDO_ERR_INVALID_SIG};
pub use common_shared::fido2::{fido_init, Fido2Assert, Fido2Error};
use common_shared::serde::export::Formatter;
use common_shared::serde::Serialize;
pub use common_shared::{
    b64d, b64e, debug_display, rand_str, Configuration, ConfigurationIdentity,
    DEFAULT_ACCESS_TOKEN_EXPIRY, DEFAULT_AUTHENTICATION_TOKEN_EXPIRY,
    DEFAULT_ID_TOKEN_VERIFICATION_EXPIRY, DEFAULT_LISTEN, DEFAULT_LISTEN_LDAP,
    DEFAULT_SESSION_INCOMPLETE_EXPIRY, DEFAULT_SESSION_VALID_EXPIRY,
};
use handlebars::Handlebars;
use http::header::{ACCESS_CONTROL_ALLOW_ORIGIN, CONTENT_TYPE};
use http::response::Builder;
use http::uri::{Authority, Parts, PathAndQuery, Scheme};
use hyper::{Body, Chunk, Response, StatusCode, Uri};
use sodiumoxide::crypto::pwhash::HashedPassword;
use std::collections::HashMap;
use std::ops::Add;
use std::sync::{Arc, Mutex};

pub fn repr(mut bytes: &[u8]) -> String {
    let mut output = String::new();
    loop {
        match std::str::from_utf8(bytes) {
            Ok(s) => {
                output.push_str(s);
                return output;
            }
            Err(e) => {
                let (good, bad) = bytes.split_at(e.valid_up_to());
                if !good.is_empty() {
                    let s = unsafe { std::str::from_utf8_unchecked(good) };
                    output.push_str(s);
                }
                if bad.is_empty() {
                    return output;
                }
                output.push_str(&format!("\\x{:02x}", bad[0]));
                bytes = &bad[1..];
            }
        }
    }
}

pub const OID2_PATH: &str = "/oid2";
pub const OIDC_DISCOVERY_PATH: &str = "/.well-known/openid-configuration";
pub const JWKS_PATH: &str = "/.well-known/jwks.json";
pub const OIDC_HUMAN_PATH: &str = "/auth";
pub const OIDC_REGISTRATION_PATH: &str = "/register";
pub const OIDC_TOKEN_PATH: &str = "/token";
pub const OIDC_USERINFO_PATH: &str = "/userinfo";

pub fn basic_response() -> Builder {
    let mut builder = Response::builder();
    builder.header(ACCESS_CONTROL_ALLOW_ORIGIN, "*");
    builder
}

pub fn html_response() -> Builder {
    let mut builder = basic_response();
    builder.header(CONTENT_TYPE, "text/html; charset=utf-8");
    builder
}

pub fn json_response() -> Builder {
    let mut builder = basic_response();
    builder.header(CONTENT_TYPE, "application/json");
    builder
}

#[derive(Debug)]
pub(crate) enum XOrError {
    MismatchedLengths,
    Empty,
}

debug_display!(XOrError);

pub(crate) fn vxor(data: &[&[u8]]) -> Result<Vec<u8>, XOrError> {
    if data.is_empty() {
        return Err(XOrError::Empty);
    }
    let mut combined = data[0].to_vec();
    for sub in data.iter().skip(1) {
        if sub.len() != combined.len() {
            return Err(XOrError::MismatchedLengths);
        }
        for (v1, v2) in combined.iter_mut().zip(*sub) {
            *v1 = *v1 ^ *v2;
        }
    }
    Ok(combined)
}

pub(crate) fn show_human_message(s: &ServerInner, msg: &str, cls: &str) -> Response<Body> {
    #[derive(Serialize)]
    struct TemplateArgs<'a> {
        error: &'a str,
        cls: &'a str,
    }
    html_response()
        .status(StatusCode::OK)
        .body(Body::from(
            s.handlebars
                .render(
                    "message.html",
                    &TemplateArgs {
                        error: msg,
                        cls: cls,
                    },
                )
                .unwrap(),
        ))
        .unwrap()
}

pub struct ReqContext {
    pub proto: Scheme,
    pub host: Authority,
    pub base_path: String,
    pub issuer: String,
    pub full_url: String,
}

pub fn join(proto: &Scheme, host: &Authority, base_path: &String, path: Option<&str>) -> String {
    let mut parts = Parts::default();
    parts.scheme = Some(proto.clone());
    parts.authority = Some(host.clone());
    parts.path_and_query = Some(
        PathAndQuery::from_str(base_path.clone().add(path.unwrap_or_else(|| "")).as_str()).unwrap(),
    );
    Uri::from_parts(parts).unwrap().to_string()
}

impl ReqContext {
    pub fn join(&self, path: Option<&str>) -> String {
        join(&self.proto, &self.host, &self.base_path, path)
    }
}

pub struct Association {
    pub identity: Option<(String, Arc<ConfigurationIdentity>)>,
    pub confirmed: bool,
}

pub struct Session {
    pub created: DateTime<Utc>,
    pub password: bool,
    pub webauthn: bool,
    pub webauthn_challenge: Option<Vec<u8>>,
    pub associations: HashMap<String, Association>,
    pub valid: bool,
    pub valid_at: DateTime<Utc>,
}

#[macro_export]
macro_rules! err_do {
    ($e:expr, $d:expr) => {
        match $e {
            Err(_e) => $d,
            Ok(v) => v,
        }
    };
}

/// Source is arg to form_urlencoded::parse, a string?
/// key is a byte string literal
/// name is the variable to be defined with the result, as an option
#[macro_export]
macro_rules! form_parse {
    ($sources:expr, $(($key:literal, $name:ident)),* $(,)?) => {
    $(
        let mut $name = None;
    )*
        for source in $sources {
            for (k, v) in url::form_urlencoded::parse(source) {
                match k.as_bytes() {
                $(
                    $key => $name = $name.or(Some(v)),
                )*
                    _ => {}
                };
            }
        }
    };
}

pub(crate) struct ServerInner {
    /// v = bool (exact match required), fn (processor)
    pub(crate) routes:
        AwfulRouter<fn(&ServerInner, ReqContext, http::request::Parts, Chunk) -> Response<Body>>,
    pub(crate) authentication_token_expiry: i64,
    pub(crate) access_token_expiry: i64,
    pub(crate) id_token_verification_expiry: i64,
    pub(crate) session_incomplete_expiry: i64,
    pub(crate) session_valid_expiry: i64,
    pub(crate) domain: String,
    pub(crate) username: String,
    pub(crate) password: Option<HashedPassword>,
    pub(crate) confirm: bool,
    pub(crate) identities: Box<[(String, Arc<ConfigurationIdentity>)]>,
    pub(crate) jwt_pubpriv_key: Vec<u8>,
    pub(crate) jwt_pub_key_hash: String,
    pub(crate) jwt_pub_key: JWTPublicKey,
    pub(crate) sign_pub_key: sodiumoxide::crypto::sign::PublicKey,
    pub(crate) sign_priv_key: sodiumoxide::crypto::sign::SecretKey,
    pub(crate) encrypt_key: sodiumoxide::crypto::secretbox::Key,
    pub(crate) clients: HashMap<String, String>,
    pub(crate) webauthn: Vec<(String, Vec<u8>)>,
    pub(crate) sessions: Mutex<Vec<(String, Arc<Mutex<Session>>)>>,
    pub(crate) handlebars: Handlebars,
    pub(crate) ldap_attributes: HashMap<String, Vec<String>>,
    pub(crate) force_https: bool,
    pub(crate) general_priv_key: Vec<u8>,
    pub(crate) general_priv_key_mask: Vec<u8>,
}

impl ServerInner {
    pub fn get_identity(&self, hash: &str) -> Option<Arc<ConfigurationIdentity>> {
        self.identities
            .iter()
            .find(|p| p.0.eq(hash))
            .map(|v| v.1.clone())
    }
}

pub trait VecExtract<T> {
    fn extract<F: Fn(&T) -> bool>(&mut self, f: F) -> VecExtractIterator<T, F>;
    fn extract_one<F: Fn(&T) -> bool>(&mut self, f: F) -> Option<T>;
}

pub struct VecExtractIterator<'a, T, F: Fn(&T) -> bool> {
    data: &'a mut Vec<T>,
    i: usize,
    f: F,
}

impl<'a, T, F: Fn(&T) -> bool> Iterator for VecExtractIterator<'a, T, F> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if self.i >= self.data.len() {
                return None;
            }
            if !(self.f)(&self.data[self.i]) {
                self.i += 1;
                continue;
            }
            return Some(self.data.remove(self.i));
        }
    }
}

impl<T> VecExtract<T> for Vec<T> {
    fn extract<F: Fn(&T) -> bool>(&mut self, f: F) -> VecExtractIterator<T, F> {
        VecExtractIterator {
            data: self,
            i: 0,
            f: f,
        }
    }

    fn extract_one<'a, F: Fn(&T) -> bool>(&mut self, f: F) -> Option<T> {
        self.extract(f).next()
    }
}

pub(crate) struct AwfulRouter<V> {
    set: regex::RegexSet,
    kvs: Vec<(&'static str, (bool, V))>,
}

impl<V> AwfulRouter<V> {
    pub(crate) fn new(values: Vec<(&'static str, (bool, V))>) -> AwfulRouter<V> {
        AwfulRouter {
            set: regex::RegexSet::new(
                &values
                    .iter()
                    .map(|v| "^".to_string() + &regex::escape(v.0) + if (v.1).0 { "$" } else { "" })
                    .collect::<Vec<String>>(),
            )
            .unwrap(),
            kvs: values,
        }
    }

    pub(crate) fn search<'a, 'b>(
        &'b self,
        s: &'a str,
    ) -> Option<(&'static str, &'a str, &'b (bool, V))> {
        self.set.matches(s).into_iter().next().map(|i| {
            let kv = &self.kvs[i];
            (kv.0, &s[kv.0.len()..], &kv.1)
        })
    }
}
