// FIXED STRUCTURES
// These may be stored in remote databases, so the serialized representation should not change

use crate::common::ServerInner;
use crate::common_shared::base64;
use crate::common_shared::rand::SeedableRng;
use base64::{decode_config as b64d_, encode_config as b64e_, URL_SAFE_NO_PAD};
use common_shared::rand::Rng;
use common_shared::sha2::Sha256;
use hmac::digest::Digest;
use rand_chacha::ChaCha20Rng;
use sodiumoxide::crypto::secretbox::xsalsa20poly1305;
use sodiumoxide::crypto::secretbox::{gen_nonce, NONCEBYTES};
use std::convert::TryInto;

pub fn b64e<T: ?Sized + AsRef<[u8]>>(t: &T) -> String {
    b64e_(t, URL_SAFE_NO_PAD)
}

pub fn b64d<T: ?Sized + AsRef<[u8]>>(input: &T) -> Result<Vec<u8>, base64::DecodeError> {
    b64d_(input, URL_SAFE_NO_PAD)
}

pub(crate) fn repeat_encrypt(s: &ServerInner, nonce_source: &[u8], data: &[u8]) -> Vec<u8> {
    let nonce = xsalsa20poly1305::Nonce::from_slice(&sha256(nonce_source)[..NONCEBYTES]).unwrap();
    let mut out = xsalsa20poly1305::seal(data, &nonce, &s.encrypt_key);
    out.extend_from_slice(&nonce.0);
    out
}

pub(crate) fn encrypt(s: &ServerInner, data: &[u8]) -> Vec<u8> {
    let nonce = gen_nonce();
    let mut out = xsalsa20poly1305::seal(data, &nonce, &s.encrypt_key);
    out.extend_from_slice(&nonce.0);
    out
}

pub(crate) fn decrypt(s: &ServerInner, data: &[u8]) -> Option<Vec<u8>> {
    if data.len() <= NONCEBYTES {
        return None;
    }
    let pivot = data.len() - NONCEBYTES;
    let nonce = xsalsa20poly1305::Nonce::from_slice(&data[pivot..]).unwrap();
    let data = &data[0..pivot];
    sodiumoxide::crypto::secretbox::open(data, &nonce, &s.encrypt_key).ok()
}

pub(crate) fn sha256(s: &[u8]) -> [u8; 32] {
    let mut hash = Sha256::default();
    hash.input(s);
    hash.result().as_slice().try_into().unwrap()
}

pub(crate) fn rand_str<R: Rng>(r: &mut R, count: usize) -> String {
    let raw = (0..count)
        .map(|_| {
            let v = r.gen_range(0, 26 + 10);
            if v < 26 {
                b'a' + v
            } else {
                b'0' + v
            }
        })
        .collect::<Vec<u8>>();
    unsafe { std::str::from_utf8_unchecked(raw.as_slice()) }.to_string()
}

pub(crate) fn seed_rand(b: &[&[u8]]) -> ChaCha20Rng {
    let mut aggregate = vec![0u8; b.iter().map(|v| v.len()).max().unwrap()];
    for i in 0..aggregate.len() {
        let v = &mut aggregate[i];
        for source in b {
            if i >= source.len() {
                continue;
            }
            *v ^= source[i];
        }
    }
    ChaCha20Rng::from_seed(sha256(aggregate.as_slice()))
}
