use crate::common::{b64e, basic_response, ServerInner};
use crate::err_do;
use crate::oid2::common::{kv, kv_body, parse_assoc_handle_create};
use crate::oid2::expose_fixed::OID2Identity;
use common_shared::serde_json;
use hyper::{Body, Chunk, Response, StatusCode};
use std::borrow::Cow;
use std::borrow::Cow::Borrowed;
use std::collections::HashMap;

pub(crate) fn process(
    s: &ServerInner,
    body_bytes: &Chunk,
    openid_ns: Cow<str>,
    openid_mode: Cow<str>,
    openid_op_endpoint: Option<Cow<str>>,
    openid_return_to: Option<Cow<str>>,
    openid_response_nonce: Option<Cow<str>>,
    openid_assoc_handle: Option<Cow<str>>,
    openid_claimed_id: Option<Cow<str>>,
    openid_identity: Option<Cow<str>>,
    openid_signed: Option<Cow<str>>,
    openid_sig: Option<Cow<str>>,
    openid_ax_mode: Option<Cow<str>>,
) -> Response<Body> {
    let respond_err = |msg: &str| {
        return basic_response()
            .status(StatusCode::BAD_REQUEST)
            .body(kv_body(&[("ns", openid_ns.as_ref()), ("error", msg)]))
            .unwrap();
    };
    let openid_claimed_id = err_do!(
        openid_claimed_id.ok_or(()),
        return respond_err("Missing openid.claimed_id")
    );
    let identity_unique = err_do!(
        openid_claimed_id.as_ref().rsplitn(2, "/").next().ok_or(()),
        return respond_err("Invalid openid.claimed_id")
    );
    let oid2_identity = err_do!(
        OID2Identity::decode(s, identity_unique).ok_or(()),
        return respond_err("Invalid openid.claimed_id")
    );
    let identity = err_do!(
        s.get_identity(&oid2_identity.0).ok_or(()),
        return respond_err("Internal error, please retry from start")
    );
    let (invalidate_assoc_handle, kt, handle_data) =
        parse_assoc_handle_create(openid_assoc_handle.clone());
    let openid_sig = err_do!(
        openid_sig.ok_or(()),
        return respond_err("Missing openid.sig")
    );
    let openid_signed = err_do!(
        openid_signed.ok_or(()),
        return respond_err("Missing openid.signed")
    );
    let mut have: HashMap<&'static str, Option<Cow<str>>> = HashMap::new();
    have.insert("op_endpoint", openid_op_endpoint);
    have.insert("return_to", openid_return_to);
    have.insert("response_nonce", openid_response_nonce);
    have.insert("assoc_handle", openid_assoc_handle.clone());
    have.insert("claimed_id", Some(Borrowed(&openid_claimed_id)));
    have.insert("identity", openid_identity);
    let mut to_sign: Vec<(&str, &str)> = vec![];
    for need in openid_signed.as_ref().split(",") {
        match have.get(need) {
            Some(v) => match v {
                Some(v) => {
                    to_sign.push((need, v.as_ref()));
                }
                None => return respond_err("Key listed in openid.signed is missing"),
            },
            None => return respond_err("Unknown key listed in openid.signed"),
        }
    }
    let mut args: Vec<(Cow<str>, Cow<str>)> = vec![
        ("ns".into(), openid_ns.clone()),
        ("mode".into(), openid_mode.clone()),
        (
            "is_valid".into(),
            Borrowed(
                if openid_sig.eq(&b64e(&kt.sign(s, &handle_data, &kv(&to_sign)))) {
                    "true"
                } else {
                    "false"
                },
            ),
        ),
    ];
    if invalidate_assoc_handle {
        args.push((
            "invalidate_handle".into(),
            Borrowed(openid_assoc_handle.as_ref().unwrap()),
        ));
    }
    for v in &openid_ax_mode {
        if v.ne("fetch_request") {
            break;
        }
        args.push(("openid.ns.ax".into(), "http://openid.net/srv/ax/1.0".into()));
        args.push(("openid.ax.mode".into(), "fetch_response".into()));
        const TYPE_PREFIX: &'static str = "openid.ax.type.";
        for (dest, typ) in url::form_urlencoded::parse(body_bytes) {
            match dest.as_ref() {
                key if key.starts_with(TYPE_PREFIX) => match identity.claims.get(typ.as_ref()) {
                    Some(serde_json::Value::String(v)) => args.push((
                        ("openid.ax.".to_string() + &key[TYPE_PREFIX.len()..]).into(),
                        v.replace("{}", &identity_unique[..20]).into(),
                    )),
                    _ => args.push((
                        ("openid.ax.count.".to_string() + &key[TYPE_PREFIX.len()..]).into(),
                        "0".into(),
                    )),
                },
                _ => {}
            }
        }
    }
    basic_response()
        .status(StatusCode::OK)
        .body(kv_body(&args))
        .unwrap()
}
