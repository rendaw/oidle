extern crate hmac;
extern crate sha1;
use crate::common::{basic_response, html_response, ReqContext, ServerInner};
use crate::form_parse;
use crate::oid2::common::kv_body;
use common_shared::serde::Serialize;
use http::request::Parts;
use hyper::{Body, Chunk, Response, StatusCode};
use std::borrow::Cow::Borrowed;
use url::Url;

mod associate;
mod common;
mod expose;
mod expose_fixed;
mod human;
mod verify;

pub(crate) fn process(
    s: &ServerInner,
    context: ReqContext,
    req: Parts,
    body_bytes: Chunk,
) -> Response<Body> {
    form_parse!(
        &[&body_bytes, req.uri.query().unwrap_or("").as_bytes()],
        (b"openid.mode", openid_mode),
        (b"openid.ns", openid_ns),
        (b"openid.return_to", openid_return_to),
        (b"openid.assoc_type", openid_assoc_type),
        (b"openid.session_type", openid_session_type),
        (b"openid.assoc_handle", openid_assoc_handle),
        (b"openid.realm", openid_realm),
        //
        // Returned (verification only)
        (b"openid.op_endpoint", openid_op_endpoint),
        (b"openid.claimed_id", openid_claimed_id),
        (b"openid.identity", openid_identity),
        (b"openid.response_nonce", openid_response_nonce),
        (b"openid.signed", openid_signed),
        (b"openid.sig", openid_sig),
        (b"openid.ax.mode", openid_ax_mode),
    );
    let openid_ns = openid_ns.unwrap_or(Borrowed("http://specs.openid.net/auth/2.0"));
    let openid_mode = match openid_mode {
        Some(v) => v,
        None => {
            // Someone opened a identifier url
            #[derive(Serialize)]
            struct TemplateArgs<'a> {
                oid2: &'a str,
            }
            return html_response()
                .status(StatusCode::OK)
                .body(Body::from(
                    s.handlebars
                        .render(
                            "root.html",
                            &TemplateArgs {
                                oid2: &context.full_url,
                            },
                        )
                        .unwrap(),
                ))
                .unwrap();
        }
    };

    let mut use_uri = Url::parse(&context.full_url).unwrap();
    use_uri.query_pairs_mut().clear();
    let t = use_uri.query().map(|v| v.to_string());
    use_uri.set_query(None);
    let endpoint = use_uri.to_string();
    use_uri.set_query(t.as_ref().map(|v| v.as_str()));
    drop(t);

    match &openid_mode {
        v if v.eq("associate") => associate::process(
            s,
            openid_ns,
            openid_mode,
            openid_assoc_type,
            openid_session_type,
        ),
        v if v.eq("checkid_immediate") => human::process(
            s,
            &req,
            &body_bytes,
            use_uri,
            endpoint,
            openid_ns,
            openid_mode,
            openid_return_to,
            openid_assoc_handle,
            openid_realm,
            true,
        ),
        v if v.eq("checkid_setup") => human::process(
            s,
            &req,
            &body_bytes,
            use_uri,
            endpoint,
            openid_ns,
            openid_mode,
            openid_return_to,
            openid_assoc_handle,
            openid_realm,
            false,
        ),
        v if v.eq("check_authentication") => verify::process(
            s,
            &body_bytes,
            openid_ns,
            openid_mode,
            openid_op_endpoint,
            openid_return_to,
            openid_response_nonce,
            openid_assoc_handle,
            openid_claimed_id,
            openid_identity,
            openid_signed,
            openid_sig,
            openid_ax_mode,
        ),
        _ => basic_response()
            .status(StatusCode::BAD_REQUEST)
            .body(kv_body(&[
                ("openid.ns", openid_ns.as_ref()),
                ("openid.error", "Unknown openid.mode"),
            ]))
            .unwrap(),
    }
}
