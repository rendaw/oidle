use crate::common::{basic_response, ServerInner};
use crate::err_do;
use crate::oid2::common::{kv_body, parse_key_type};
use crate::oid2::expose::{format_assoc_handle, generate_assoc_handle, get_mac_key_str};
use hyper::{Body, Response, StatusCode};
use std::borrow::Cow;
use std::borrow::Cow::Borrowed;

pub(crate) fn process(
    s: &ServerInner,
    openid_ns: Cow<str>,
    openid_mode: Cow<str>,
    openid_assoc_type: Option<Cow<str>>,
    openid_session_type: Option<Cow<str>>,
) -> Response<Body> {
    let openid_assoc_type = openid_assoc_type.unwrap_or(Borrowed("HMAC-SHA256"));
    let kt = err_do!(
        parse_key_type(openid_assoc_type.as_ref()).ok_or(()),
        return basic_response()
            .status(StatusCode::BAD_REQUEST)
            .body(kv_body(&[
                ("ns", openid_ns.as_ref()),
                ("mode", "error"),
                ("error", "Unsupported association type"),
                ("error_code", "unsupported-type"),
                ("assoc_type", openid_assoc_type.as_ref()),
                ("session_type", SESSION_TYPE),
            ]))
            .unwrap()
    );
    const SESSION_TYPE: &str = "no-encryption";
    let openid_session_type = openid_session_type.unwrap_or(Borrowed(SESSION_TYPE));

    if openid_session_type.ne(SESSION_TYPE) {
        return basic_response()
            .status(StatusCode::BAD_REQUEST)
            .body(kv_body(&[
                ("ns", openid_ns.as_ref()),
                ("mode", "error"),
                ("error", "No session encryption supported"),
                ("error_code", "unsupported-type"),
                ("assoc_type", openid_assoc_type.as_ref()),
                ("session_type", SESSION_TYPE),
            ]))
            .unwrap();
    }
    let handle_data = generate_assoc_handle(&kt);
    basic_response()
        .status(StatusCode::OK)
        .body(kv_body(&[
            ("ns", openid_ns.as_ref()),
            ("mode", openid_mode.as_ref()),
            ("assoc_handle", &format_assoc_handle(&kt, &handle_data)),
            ("assoc_type", openid_assoc_type.as_ref()),
            ("expires_in", "1209600"),
            ("session_type", openid_session_type.as_ref()),
            ("mac_key", &get_mac_key_str(s, &handle_data)),
        ]))
        .unwrap()
}
