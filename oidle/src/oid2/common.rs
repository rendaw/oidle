extern crate enum_dispatch;
use super::hmac::digest::Digest;
use super::hmac::Mac;
use crate::common::{b64d, ServerInner};
use crate::err_do;
use crate::oid2::expose::{generate_assoc_handle, get_mac_key};
use common_shared::sha2;
use hyper::Body;
use std::borrow::Cow;
use std::ops::Deref;

pub(crate) fn kv<X: AsRef<str>, Y: AsRef<str>>(pairs: &[(X, Y)]) -> Vec<u8> {
    let mut alloc = Vec::with_capacity(
        pairs
            .iter()
            .map(|v| v.0.as_ref().as_bytes().len() + v.1.as_ref().as_bytes().len() + 2)
            .sum(),
    );
    for (k, v) in pairs {
        alloc.extend_from_slice(k.as_ref().as_bytes());
        alloc.push(b':');
        alloc.extend_from_slice(v.as_ref().as_bytes());
        alloc.push(b'\n');
    }
    alloc
}

pub(crate) fn kv_body<X: AsRef<str>, Y: AsRef<str>>(pairs: &[(X, Y)]) -> Body {
    Body::from(kv(pairs))
}

pub(crate) enum AssocType {
    SHA1,
    SHA256,
}

impl AssocType {
    pub(crate) fn openid2_type(&self) -> &'static str {
        match self {
            AssocType::SHA1 => "HMAC-SHA1",
            AssocType::SHA256 => "HMAC-SHA256",
        }
    }

    pub(crate) fn key_len(&self) -> usize {
        match self {
            AssocType::SHA1 => sha1::Sha1::output_size(),
            AssocType::SHA256 => sha2::Sha256::output_size(),
        }
    }

    pub(crate) fn sign(&self, s: &ServerInner, handle_data: &[u8], body: &[u8]) -> Vec<u8> {
        let mac_key = get_mac_key(s, &handle_data);
        match self {
            AssocType::SHA1 => {
                type DIGEST = sha1::Sha1;
                type HMAC = hmac::Hmac<DIGEST>;
                let mut state = HMAC::new_varkey(&mac_key).unwrap();
                state.input(body);
                state.result().code().deref().to_vec()
            }
            AssocType::SHA256 => {
                type DIGEST = sha2::Sha256;
                type HMAC = hmac::Hmac<DIGEST>;
                let mut state = HMAC::new_varkey(&mac_key).unwrap();
                state.input(body);
                state.result().code().deref().to_vec()
            }
        }
    }
}

pub(crate) fn parse_key_type(k: &str) -> Option<AssocType> {
    match k {
        "HMAC-SHA1" => Some(AssocType::SHA1),
        "HMAC-SHA256" => Some(AssocType::SHA256),
        _ => None,
    }
}

pub(crate) fn parse_assoc_handle(
    openid_assoc_handle: Option<Cow<str>>,
) -> Option<(AssocType, Vec<u8>)> {
    let mut sign: Option<(AssocType, Vec<u8>)> = None;
    for openid_assoc_handle in openid_assoc_handle {
        let (assoc_type, key_xor) = {
            let mut parts = openid_assoc_handle.splitn(2, ".");
            (
                err_do!(parts.next().ok_or(()), {
                    break;
                }),
                err_do!(parts.next().ok_or(()), {
                    break;
                }),
            )
        };
        let kt = err_do!(parse_key_type(assoc_type).ok_or(()), {
            break;
        });
        let handle_data = err_do!(
            b64d(&key_xor)
                .ok()
                .filter(|v| v.len() == kt.key_len())
                .ok_or(()),
            {
                break;
            }
        );
        sign = Some((kt, handle_data));
    }
    sign
}

pub(crate) fn parse_assoc_handle_create(
    openid_assoc_handle: Option<Cow<str>>,
) -> (bool, AssocType, Vec<u8>) {
    let sign: Option<(AssocType, Vec<u8>)> = parse_assoc_handle(openid_assoc_handle);
    let invalidate_assoc_handle = sign.is_none();
    let sign =
        sign.unwrap_or_else(|| (AssocType::SHA256, generate_assoc_handle(&AssocType::SHA256)));
    (invalidate_assoc_handle, sign.0, sign.1)
}
