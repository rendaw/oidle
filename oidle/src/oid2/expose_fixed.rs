// FIXED STRUCTURES
// These may be stored in remote databases, so the serialized representation should not change
use crate::common::ServerInner;
use crate::fixed::{b64d, b64e, decrypt, repeat_encrypt};
use std::borrow::Cow;
use std::borrow::Cow::Owned;

// Identity
// ____
// Requirements:
// - Opaque to client
// - Doesn't expose relation to any other ids on other sites
// - We can look up the log in identity from it for use in other endpoints
//
// Implementation:
// * Use symmetric encryption with our private key to encrypt the identity hash
// * Nonce is based on the realm, which will be unique per site and produce a repeatable unique encryption
// * Assumes internal identity is b64 encoded bytes (hash result) which is decrypted first to reduce id size
pub(crate) struct OID2Identity<'a>(pub(crate) Cow<'a, str>);

impl<'a> OID2Identity<'a> {
    pub(crate) fn decode<'b>(s: &ServerInner, data: &str) -> Option<OID2Identity<'b>> {
        b64d(data)
            .ok()
            .and_then(|v| decrypt(s, &v))
            .map(|v| OID2Identity(Owned(b64e(&v))))
    }

    pub(crate) fn encode(&self, s: &ServerInner, realm: &str) -> String {
        b64e(&repeat_encrypt(
            s,
            realm.as_bytes(),
            &b64d(self.0.as_ref()).unwrap(),
        ))
    }
}
