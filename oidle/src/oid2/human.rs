extern crate enum_dispatch;
use crate::authenticate as ax;
use crate::authenticate::progress_authentication;
use crate::common::rand_str;
use crate::common::{b64e, basic_response, show_human_message, ServerInner};
use crate::err_do;
use crate::oid2::common::{kv, parse_assoc_handle_create};
use crate::oid2::expose::format_assoc_handle;
use chrono::{SecondsFormat, Utc};
use common_shared::rand::thread_rng;
use enum_dispatch::enum_dispatch;
use http::header::LOCATION;
use http::request::Parts;
use hyper::{Body, Chunk, Response, StatusCode};
use std::borrow::Cow;
use std::borrow::Cow::{Borrowed, Owned};
use url::Url;

pub(crate) fn process(
    s: &ServerInner,
    req: &Parts,
    body_bytes: &Chunk,
    mut use_uri: Url,
    endpoint: String,
    openid_ns: Cow<str>,
    openid_mode: Cow<str>,
    openid_return_to: Option<Cow<str>>,
    openid_assoc_handle: Option<Cow<str>>,
    openid_realm: Option<Cow<str>>,
    dont_prompt: bool,
) -> Response<Body> {
    // Validate parameters in general
    #[enum_dispatch]
    trait RespondClientOID2 {
        fn normal(&self, args: &[(&str, Cow<str>)]) -> Response<Body>;
        fn error(&self, desc: Cow<str>) -> Response<Body>;
    }

    struct RespondClientReturnTo<'a> {
        return_to: &'a Url,
        openid_ns: Cow<'a, str>,
    }
    impl<'a> RespondClientOID2 for RespondClientReturnTo<'a> {
        fn normal(&self, args: &[(&str, Cow<str>)]) -> Response<Body> {
            let mut url = self.return_to.clone();
            url.query_pairs_mut().extend_pairs(args);
            basic_response()
                .status(StatusCode::FOUND)
                .header(LOCATION, url.to_string())
                .body(Body::empty())
                .unwrap()
        }

        fn error(&self, desc: Cow<str>) -> Response<Body> {
            self.normal(&[
                ("openid.ns", self.openid_ns.clone()),
                ("openid.mode", Borrowed("error")),
                ("openid.error", desc),
            ])
        }
    }

    struct RespondClientFallback<'a> {
        s: &'a ServerInner,
    }
    impl<'a> RespondClientOID2 for RespondClientFallback<'a> {
        fn normal(&self, args: &[(&str, Cow<str>)]) -> Response<Body> {
            show_human_message(
                self.s,
                &format!(
                    "Code: {}",
                    args.iter().find(|v| v.0.eq("id_token")).unwrap().1
                ),
                "",
            )
        }

        fn error(&self, desc: Cow<str>) -> Response<Body> {
            show_human_message(self.s, desc.as_ref(), "msg_err")
        }
    }

    #[enum_dispatch(RespondClientOID2)]
    enum RespondClientImpls<'a> {
        A(RespondClientReturnTo<'a>),
        B(RespondClientFallback<'a>),
    }

    let parsed_return_to: Option<Url>;
    let (openid_realm, respond_client): (_, RespondClientImpls) = match &openid_return_to {
        Some(v) => {
            parsed_return_to = Some(err_do!(
                Url::parse(v).ok().ok_or(()),
                return show_human_message(s, "Invalid openid.return_to", "msg_err")
            ));

            let return_to_host = err_do!(
                parsed_return_to.as_ref().unwrap().host().ok_or(()),
                return show_human_message(s, "Invalid openid.return_to", "msg_err")
            )
            .to_string();

            let respond_client = RespondClientReturnTo {
                return_to: parsed_return_to.as_ref().unwrap(),
                openid_ns: openid_ns.clone(),
            };

            (
                match openid_realm {
                    Some(v) => {
                        if v.starts_with("*.") && !return_to_host.ends_with(&v[2..]) {
                            return respond_client.error(Borrowed("Invalid realm"));
                        }
                        Some(v)
                    }
                    None => Some(Owned(return_to_host)),
                },
                respond_client.into(),
            )
        }
        None => (None, RespondClientFallback { s: s }.into()),
    };

    // Prep tools
    if dont_prompt {
        use_uri.query_pairs_mut().append_pair("prompt", "none");
    }
    use_uri
        .query_pairs_mut()
        .extend_pairs(&[("openid.mode", openid_mode.clone())]);
    for v in &openid_return_to {
        use_uri
            .query_pairs_mut()
            .append_pair("openid.return_to", v.as_ref());
    }
    for v in &openid_realm {
        use_uri
            .query_pairs_mut()
            .append_pair("openid.realm", v.as_ref());
    }
    for v in &openid_assoc_handle {
        use_uri
            .query_pairs_mut()
            .append_pair("openid.assoc_handle", v.as_ref());
    }

    let now = Utc::now();

    // Human login
    let done = match progress_authentication(
        s,
        &req,
        &body_bytes,
        now,
        openid_realm.clone(),
        use_uri,
        if dont_prompt {
            Some(Borrowed("none"))
        } else {
            None
        },
        None,
    ) {
        Ok(ax::Resp::Continue(resp)) => return resp,
        Err(ax::Error::Client(rej)) => {
            return respond_client.error(rej.desc);
        }
        Ok(ax::Resp::Done(d)) => d,
    };

    if done.identity.pairwise_subject && openid_realm.is_none() {
        return show_human_message(
            s,
            "You've enabled pairwise identity but this login does not provide pair information.",
            "msg_err",
        );
    }

    // Prepare signature and respond
    let (invalidate_assoc_handle, kt, handle_data) = parse_assoc_handle_create(openid_assoc_handle);
    let openid_assoc_handle = format_assoc_handle(&kt, &handle_data);

    let identity = endpoint.clone()
        + "/"
        + &crate::oid2::expose_fixed::OID2Identity(Borrowed(done.identity_key.as_str())).encode(
            s,
            openid_realm
                .filter(|_| done.identity.pairwise_subject)
                .as_ref()
                .map(|v| v.as_ref())
                .unwrap_or("invalid"),
        );
    let nonce = now.to_rfc3339_opts(SecondsFormat::Secs, true) + &rand_str(&mut thread_rng(), 10);
    let mut to_sign = vec![
        ("op_endpoint", identity.as_str()),
        ("response_nonce", &nonce.as_str()),
        ("claimed_id", identity.as_ref()),
        ("identity", identity.as_str()),
        ("assoc_handle", openid_assoc_handle.as_str()),
    ];
    for v in &openid_return_to {
        to_sign.push(("return_to", v.as_ref()));
    }
    let sig = Owned(b64e(&kt.sign(s, &handle_data, &kv(&to_sign))));
    let signed_keys = Owned(to_sign.iter().map(|v| v.0).collect::<Vec<&str>>().join(","));
    let mut respond_args = vec![
        ("openid.ns", openid_ns.clone()),
        ("openid.mode", Borrowed("id_res")),
        ("openid.op_endpoint", Borrowed(&identity)),
        ("openid.claimed_id", Borrowed(&identity)),
        ("openid.identity", Borrowed(&identity)),
        ("openid.response_nonce", Owned(nonce)),
        ("openid.signed", signed_keys),
        ("openid.sig", sig),
        (
            "openid.assoc_handle",
            Borrowed(openid_assoc_handle.as_str()),
        ),
    ];
    for v in &openid_return_to {
        respond_args.push(("openid.return_to", v.clone()));
    }
    if invalidate_assoc_handle {
        respond_args.push((
            "openid.invalidate_handle",
            Borrowed(openid_assoc_handle.as_str()),
        ));
    }
    respond_client.normal(&respond_args)
}
