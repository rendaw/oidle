use super::hmac::digest::Digest;
use crate::common::{vxor, ServerInner};
use crate::fixed::b64e;
use crate::oid2::common::AssocType;
use common_shared::sha2;

// Association handles + mac keys
// ____
// mac key requirements
// - non-opaque data for client
// - not stored, can be regenerated locally
// assoc handle requirements
// - opaque for client
// - returned to us, so can store info about mac key
//
// Implementation:
// Derive mac key from our effective public key (private key ^ mask) by xoring with the assoc_handle which is just random bytes.  The client could get the effective public key back but this should not be sensitive data.
pub(crate) fn get_mac_key(s: &ServerInner, assoc_handle_data: &[u8]) -> Vec<u8> {
    vxor(&[
        &assoc_handle_data,
        &s.general_priv_key[0..assoc_handle_data.len()],
        &s.general_priv_key_mask[0..assoc_handle_data.len()],
    ])
    .unwrap()
}

pub(crate) fn get_mac_key_str(s: &ServerInner, assoc_handle_data: &[u8]) -> String {
    b64e(&get_mac_key(s, assoc_handle_data))
}

pub(crate) fn generate_assoc_handle(kt: &AssocType) -> Vec<u8> {
    sodiumoxide::randombytes::randombytes(match kt {
        AssocType::SHA1 => sha1::Sha1::output_size(),
        AssocType::SHA256 => sha2::Sha256::output_size(),
    })
}

pub(crate) fn format_assoc_handle(kt: &AssocType, key: &[u8]) -> String {
    format!("{}.{}", kt.openid2_type(), &b64e(key))
}
