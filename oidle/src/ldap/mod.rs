extern crate log;

use std::convert::TryInto;

use log::error;
use tokio::net::TcpStream;
use tokio::prelude::future::Either;
use tokio::prelude::{future, Future};

use crate::Server;
use std::borrow::Cow::{Borrowed, Owned};
use std::borrow::{Borrow, Cow};

#[derive(Debug)]
enum LDAPError {
    // Local errors
    // Remote errors
    MessageTooBig,
    EOS,
    NotBind(u8),
    UnimplementedMethod(u8),
    MissingBindVersion,
    SeqNotANumber,
    UnsupportedBindVersion,
    BindTypeNotString,
    BindMethodNotSimple,
    StdIOError(std::io::Error),
    InvalidUTF8Value(std::string::FromUtf8Error),
    // Non errors
    Die,
}

impl From<std::io::Error> for LDAPError {
    fn from(e: std::io::Error) -> Self {
        LDAPError::StdIOError(e)
    }
}

impl From<std::string::FromUtf8Error> for LDAPError {
    fn from(e: std::string::FromUtf8Error) -> Self {
        LDAPError::InvalidUTF8Value(e)
    }
}

type LDAPResult<T> = Result<T, LDAPError>;

struct ReadElements<'a> {
    at: usize,
    data: &'a [u8],
}

impl ReadElements<'_> {
    fn many(&mut self, count: usize) -> LDAPResult<&[u8]> {
        if self.at + count > self.data.len() {
            return Err(LDAPError::EOS);
        }
        let out = &self.data[self.at..self.at + count];
        self.at += count;
        Ok(out)
    }

    fn one(&mut self) -> LDAPResult<u8> {
        Ok(self.many(1)?[0])
    }

    fn read_element(&mut self) -> LDAPResult<(u8, ReadElements)> {
        let _type = self.one()?;
        let length = self.one()? as usize;
        Ok((
            _type,
            ReadElements {
                at: 0,
                data: self.many(if length >= 0x80 {
                    (length & 0x80 - 1) as usize
                } else {
                    length
                })?,
            },
        ))
    }

    fn at_end(&self) -> bool {
        self.at == self.data.len()
    }
}

struct WriteElements {
    e: Vec<u8>,
}

impl WriteElements {
    fn add(mut self, t: u8, data: &[u8]) -> Self {
        self.e.push(t);
        if data.len() >= 0x80 {
            let len = data.len() as u64;
            self.e.push(0x80 | std::mem::size_of_val(&len) as u8);
            self.e.extend_from_slice(&len.to_be_bytes());
        } else {
            self.e.push(data.len() as u8);
        };
        self.e.extend(data);
        self
    }

    fn done(self) -> Vec<u8> {
        self.e
    }
}

fn write_elements() -> WriteElements {
    WriteElements { e: vec![] }
}

type Fut<T> = Box<dyn Future<Item = T, Error = LDAPError> + Send>;

fn read_header(conn: TcpStream) -> Fut<(TcpStream, Vec<u8>)> {
    Box::new(
        tokio::io::read_exact(conn, vec![0; 2])
            .map_err(|e| match e {
                _ if e.kind() == std::io::ErrorKind::UnexpectedEof => LDAPError::Die,
                e => e.into(),
            })
            .and_then(|(conn, buf)| {
                match (|| {
                    if buf[1] >= 128 {
                        let length = (buf[1] & 0x7f) as usize;
                        if length > 2 {
                            return Err(LDAPError::MessageTooBig);
                        }
                        Ok(Either::A(
                            tokio::io::read_exact(conn, vec![0; length])
                                .map_err(|e| e.into())
                                .and_then(|(conn, buf)| {
                                    let length = if buf.len() == 1 {
                                        u8::from_be_bytes(buf.as_slice().try_into().unwrap())
                                            as usize
                                    } else {
                                        u16::from_be_bytes(buf.as_slice().try_into().unwrap())
                                            as usize
                                    };
                                    tokio::io::read_exact(conn, vec![0; length])
                                        .map_err(|e| e.into())
                                }),
                        ))
                    } else {
                        Ok(Either::B(
                            tokio::io::read_exact(conn, vec![0; buf[1] as usize])
                                .map_err(|e| e.into()),
                        ))
                    }
                })() {
                    Ok(f) => Either::A(f),
                    Err(e) => Either::B(future::err(e)),
                }
            }),
    )
}

fn handle_bind(
    server: Server,
    conn: TcpStream,
    seq: Vec<u8>,
    mut cur: ReadElements,
) -> LDAPResult<Fut<()>> {
    {
        // Bind version
        let (t, mut cur) = cur.read_element()?;
        if t != 0x2 {
            return Err(LDAPError::MissingBindVersion);
        }
        if cur.one()? != 3 || !cur.at_end() {
            return Err(LDAPError::UnsupportedBindVersion);
        }
    }
    let name;
    {
        let (t, cur) = cur.read_element()?;
        if t != 0x4 {
            return Err(LDAPError::BindTypeNotString);
        }
        name = cur.data.to_vec();
    }
    let pass;
    {
        let (t, cur) = cur.read_element()?;
        if t != 0x80 {
            return Err(LDAPError::BindMethodNotSimple);
        }
        pass = cur.data;
    }
    if String::from_utf8(name)?.ne(&server.0.username)
        || !sodiumoxide::crypto::pwhash::pwhash_verify(server.0.password.as_ref().unwrap(), pass)
    {
        Ok(Box::new(
            tokio::io::write_all(
                conn,
                write_elements()
                    .add(
                        0x30,
                        &write_elements()
                            .add(0x2, &seq)
                            .add(
                                0x61, // Bind response
                                &write_elements()
                                    .add(0xa, &[0x20]) // Result -> Not OK
                                    .add(0x4, &[]) // BindDN
                                    .add(0x4, &[]) // Error
                                    .done(),
                            )
                            .done(),
                    )
                    .done(),
            )
            .map_err(|e| e.into())
            .and_then(|(conn, _)| expect_bind(server, conn)),
        ))
    } else {
        Ok(Box::new(
            tokio::io::write_all(
                conn,
                write_elements()
                    .add(
                        0x30,
                        &write_elements()
                            .add(0x2, &seq)
                            .add(
                                0x61, // Bind response
                                &write_elements()
                                    .add(0xa, &[0x0]) // Result -> OK
                                    .add(0x4, &[]) // Bind DN
                                    .add(0x4, &[]) // Error
                                    .done(),
                            )
                            .done(),
                    )
                    .done(),
            )
            .map_err(|e| e.into())
            .and_then(|(conn, _)| expect_post_bind(server, conn)),
        ))
    }
}

fn expect_bind(server: Server, conn: TcpStream) -> Fut<()> {
    Box::new(
        read_header(conn)
            .map_err(|e| e.into())
            .and_then(|(conn, buf)| {
                match (|| {
                    let mut cur = ReadElements {
                        at: 0,
                        data: buf.as_slice(),
                    };
                    let (seq_t, seq_cur) = cur.read_element()?;
                    if seq_t != 0x2 {
                        return Err(LDAPError::SeqNotANumber);
                    }
                    let seq = seq_cur.data.to_vec();
                    let (t, cur) = cur.read_element()?; // Actual payload
                    if t != 0x60 {}
                    match t {
                        0x42 => Err(LDAPError::Die),
                        0x60 => handle_bind(server, conn, seq, cur),
                        _ => Err(LDAPError::NotBind(t)),
                    }
                })() {
                    Ok(f) => Either::A(f),
                    Err(e) => Either::B(future::err(e)),
                }
            }),
    )
}

fn handle_search(
    server: Server,
    conn: TcpStream,
    seq: Vec<u8>,
    mut cur: ReadElements,
) -> LDAPResult<Fut<()>> {
    cur.read_element()?; // Base
    cur.read_element()?; // Scope
    cur.read_element()?; // Deref aliases
    cur.read_element()?; // Size limit
    cur.read_element()?; // Time limit
    cur.read_element()?; // Types only
    cur.read_element()?; // Filter

    let mut attribute_keys: Vec<Cow<String>> = vec![];
    let mut all_attributes = false;
    {
        // Attributes
        let (_, mut cur) = cur.read_element()?;
        if cur.at_end() {
            all_attributes = true;
        }
        while !cur.at_end() {
            let key = cur.read_element()?.1;
            if key.at_end() {
                continue;
            } else if key.data == [b'*'] {
                all_attributes = true;
            } else {
                attribute_keys.push(Owned(String::from_utf8(key.data.to_vec())?));
            }
        }
    }
    if all_attributes {
        attribute_keys.clear();
        attribute_keys.extend(server.0.ldap_attributes.keys().map(Borrowed));
    }

    // Write
    let mut attributes = write_elements();
    for key in attribute_keys {
        let values = server.0.ldap_attributes.get::<String>(key.borrow());
        if values.is_none() {
            continue;
        }
        let values = values.unwrap();
        let mut value_elements = write_elements();
        for value in values {
            value_elements = value_elements.add(0x4, value.as_bytes());
        }
        attributes = attributes.add(
            0x30,
            &write_elements()
                .add(0x4, key.as_bytes())
                .add(0x31, &value_elements.done())
                .done(),
        );
    }
    Ok(Box::new(
        tokio::io::write_all(
            conn,
            write_elements()
                .add(
                    0x30, // Result
                    &write_elements()
                        .add(0x2, &seq)
                        .add(
                            0x64,
                            &write_elements()
                                .add(0x4, server.0.username.as_bytes()) // Result DN
                                .add(0x30, &attributes.done())
                                .done(),
                        )
                        .done(),
                )
                .add(
                    0x30, // Done
                    &write_elements()
                        .add(0x2, &seq)
                        .add(
                            0x65,
                            &write_elements()
                                .add(0xa, &[0x0]) // OK
                                .add(0x4, &[0x0]) // Matched dn -> empty
                                .add(0x4, &[0x0]) // Error -> empty
                                .done(),
                        )
                        .done(),
                )
                .done(),
        )
        .map_err(|e| e.into())
        .and_then(|(conn, _)| expect_post_bind(server, conn)),
    ))
}

fn expect_post_bind(server: Server, conn: TcpStream) -> Fut<()> {
    Box::new(read_header(conn).and_then(|(conn, buf)| {
        match (|| {
            // Read
            let mut cur = ReadElements {
                at: 0,
                data: buf.as_slice(),
            };
            let (seq_t, seq_cur) = cur.read_element()?;
            if seq_t != 0x2 {
                return Err(LDAPError::SeqNotANumber);
            }
            let seq = seq_cur.data.to_vec();
            let (t, cur) = cur.read_element()?; // Actual payload
            match t {
                0x42 => Err(LDAPError::Die),
                0x60 => handle_bind(server, conn, seq, cur),
                0x63 => handle_search(server, conn, seq, cur),
                _ => Err(LDAPError::UnimplementedMethod(t)),
            }
        })() {
            Ok(f) => Either::A(f),
            Err(e) => Either::B(future::err(e)),
        }
    }))
}

pub(crate) fn process(
    server: Server,
    runtime: tokio::runtime::current_thread::Handle,
    conn: TcpStream,
) {
    runtime
        .spawn(expect_bind(server, conn).map_err(|e| match e {
            LDAPError::Die => (),
            e => {
                error!("Error in LDAP connection: {:?}", e);
            }
        }))
        .unwrap();
}
