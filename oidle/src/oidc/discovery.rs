use crate::common::{
    json_response, ReqContext, ServerInner, JWKS_PATH, OIDC_HUMAN_PATH, OIDC_REGISTRATION_PATH,
    OIDC_TOKEN_PATH, OIDC_USERINFO_PATH,
};
use common_shared::serde::Serialize;
use common_shared::serde_json;
use common_shared::serde_with_macros::skip_serializing_none;
use http::request::Parts;
use hyper::{Body, Chunk, Response, StatusCode};

pub(crate) fn process(
    _s: &ServerInner,
    context: ReqContext,
    _req: Parts,
    _body_bytes: Chunk,
) -> Response<Body> {
    #[skip_serializing_none]
    #[derive(Serialize)]
    struct OIDConfigurationResponse<'a> {
        issuer: &'a String,
        registration_endpoint: String,
        authorization_endpoint: String,
        token_endpoint: String,
        token_endpoint_auth_methods_supported: Vec<&'static str>,
        userinfo_endpoint: String,
        // revocation_endpoint: String,
        jwks_uri: String,
        response_types_supported: Vec<&'static str>,
        subject_types_supported: Vec<&'static str>,
        id_token_signing_alg_values_supported: Vec<&'static str>,
        scopes_supported: Vec<&'static str>,
        claims_supported: Vec<&'static str>,
    }

    json_response()
        .status(StatusCode::OK)
        .body(Body::from(
            serde_json::to_string(&OIDConfigurationResponse {
                issuer: &context.issuer,
                registration_endpoint: context.join(Some(OIDC_REGISTRATION_PATH)),
                authorization_endpoint: context.join(Some(OIDC_HUMAN_PATH)),
                token_endpoint: context.join(Some(OIDC_TOKEN_PATH)),
                token_endpoint_auth_methods_supported: vec![
                    "client_secret_basic",
                    "client_secret_post",
                ],
                userinfo_endpoint: context.join(Some(OIDC_USERINFO_PATH)),
                jwks_uri: context.join(Some(JWKS_PATH)),
                response_types_supported: vec!["code", "id_token", "token"],
                subject_types_supported: vec!["public", "pairwise"],
                id_token_signing_alg_values_supported: vec!["RS256"],
                scopes_supported: vec!["openid", "email", "profile", "address", "phone"],
                claims_supported: vec!["name", "email"],
            })
            .unwrap(),
        ))
        .unwrap()
}
