extern crate serde_with_macros;
use crate::common::{json_response, ReqContext, ServerInner};
use common_shared::serde::{Deserialize, Serialize};
use common_shared::serde_json;
use common_shared::serde_with_macros::skip_serializing_none;
use http::request::Parts;
use hyper::{Body, Chunk, Response, StatusCode};

#[derive(Deserialize)]
struct Request {
    redirect_uris: Vec<String>,
    sector_identifier_uri: Option<String>,
    response_types: Option<Vec<String>>,
    grant_types: Option<Vec<String>>,
    token_endpoint_auth_method: Option<String>,
}

#[skip_serializing_none]
#[derive(Serialize)]
struct ProcessResponse<'a> {
    client_id: &'a str,
    client_secret: &'a str,
}

#[skip_serializing_none]
#[derive(Serialize)]
struct OIDTokenErrorResponse {
    error: String,
    error_description: String,
}

pub(crate) fn process(
    s: &ServerInner,
    _context: ReqContext,
    _req: Parts,
    body_bytes: Chunk,
) -> Response<Body> {
    let respond_err = |error: &str, desc: &str| -> Response<Body> {
        json_response()
            .status(StatusCode::BAD_REQUEST)
            .body(Body::from(
                serde_json::to_string(&OIDTokenErrorResponse {
                    error: error.to_string(),
                    error_description: desc.to_string(),
                })
                .unwrap(),
            ))
            .unwrap()
    };
    let req: Request = match serde_json::from_slice(&body_bytes) {
        Ok(v) => v,
        Err(_) => return respond_err("invalid_client_metadata", "Invalid request JSON"),
    };
    if !req
        .grant_types
        .map(|v| {
            for e in v {
                match e.as_str() {
                    "authorization_code" => (),
                    "implicit" => (),
                    _ => return false,
                }
            }
            true
        })
        .unwrap_or(false)
    {
        return respond_err("unsupported_metadata", "Unsupported grant types");
    }
    if !req
        .response_types
        .map(|v| {
            for e in v {
                match e.as_str() {
                    "code" => (),
                    "id_token" => (),
                    "token id_token" => (),
                    _ => return false,
                }
            }
            true
        })
        .unwrap_or(false)
    {
        return respond_err("unsupported_metadata", "Unsupported response_types");
    }
    if !req
        .token_endpoint_auth_method
        .map(|v| {
            ["client_secret_basic", "client_secret_post"]
                .iter()
                .any(|e| e.eq(&v))
        })
        .unwrap_or(false)
    {
        return respond_err(
            "unsupported_metadata",
            "Unsupported token_endpoint_auth_method",
        );
    }
    let (client_id, client_secret) = crate::oidc::expose::OIDClientID {
        redirect_uris: req.redirect_uris,
        sector_identifier_uri: req.sector_identifier_uri,
    }
    .encode(s);
    json_response()
        .status(StatusCode::CREATED)
        .body(Body::from(
            serde_json::to_string(&ProcessResponse {
                client_id: &client_id,
                client_secret: &client_secret,
            })
            .unwrap(),
        ))
        .unwrap()
}
