extern crate serde_with_macros;
use crate::common::{json_response, ReqContext, ServerInner};
use crate::form_parse;
use crate::oidc::common::{
    generate_access_token, generate_id_token, AuthenticationState, OIDTokenResponse,
};
use crate::oidc::expose_fixed::generate_subject;
use chrono::{TimeZone, Utc};
use common_shared::base64;
use common_shared::serde::Serialize;
use common_shared::serde_json;
use common_shared::serde_with_macros::skip_serializing_none;
use http::header::AUTHORIZATION;
use http::request::Parts;
use hyper::{Body, Chunk, Response, StatusCode};
use std::borrow::Borrow;
use std::borrow::Cow::Owned;

#[skip_serializing_none]
#[derive(Serialize)]
struct OIDTokenErrorResponse {
    error: String,
    error_description: String,
}

pub(crate) fn process(
    s: &ServerInner,
    context: ReqContext,
    req: Parts,
    body_bytes: Chunk,
) -> Response<Body> {
    // Check inputs
    let respond_err = |error: &str, desc: &str| -> Response<Body> {
        json_response()
            .status(StatusCode::BAD_REQUEST)
            .body(Body::from(
                serde_json::to_string(&OIDTokenErrorResponse {
                    error: error.to_string(),
                    error_description: desc.to_string(),
                })
                .unwrap(),
            ))
            .unwrap()
    };
    form_parse!(
        &[&body_bytes],
        (b"client_id", client_id),
        (b"client_secret", client_secret),
        (b"grant_type", grant_type),
        (b"code", code),
    );
    let (client_id, client_secret) = if client_id.is_none() || client_secret.is_none() {
        const TAG: &'static str = "basic";
        req.headers
            .get(AUTHORIZATION)
            .and_then(|basic| basic.to_str().ok())
            .filter(|basic| basic.to_ascii_lowercase().starts_with(TAG))
            .and_then(|basic| base64::decode_config(&basic[TAG.len() + 1..], base64::STANDARD).ok())
            .and_then(|basic| String::from_utf8(basic).ok())
            .map(|basic| {
                let mut basic = basic.split(":");
                (
                    basic.next().map(|v| Owned(v.to_string())),
                    basic.next().map(|v| Owned(v.to_string())),
                )
            })
            .unwrap_or((None, None))
    } else {
        (client_id, client_secret)
    };
    let client_id = match client_id {
        Some(v) => v,
        None => return respond_err("invalid_request", "Missing client_id"),
    };
    let client_secret = match client_secret {
        Some(v) => v,
        None => return respond_err("invalid_request", "Missing client_secret"),
    };
    let preregistered_client = match s.clients.get::<str>(client_id.borrow()) {
        Some(s) => {
            if s.ne(&client_secret) {
                return respond_err("invalid_request", "Invalid client_id/secret");
            }
            true
        }
        None => false,
    };
    match grant_type {
        Some(v) => {
            if v.eq("authorization_code") {
                // OK
            } else {
                return respond_err(
                    "invalid_request",
                    "Only authorization_code supported for grant_type",
                );
            }
        }
        None => return respond_err("invalid_request", "Missing grant_type"),
    };
    let state = match code.and_then(|v| AuthenticationState::decode(s, v.as_ref())) {
        Some(v) => v,
        None => return respond_err("invalid_grant", "Missing or invalid code"),
    };
    if !preregistered_client {
        let client = {
            match crate::oidc::expose::OIDClientID::decode(s, &client_id, &client_secret) {
                Some(v) => v,
                None => return respond_err("invalid_request", "Invalid client_id/secret"),
            }
        };
        if !client
            .redirect_uris
            .iter()
            .any(|v| v.eq(&state.redirect_uri))
        {
            return respond_err("invalid_request", "Invalid client_id/secret");
        }
    }
    let now = Utc::now().timestamp();
    if now - state.timestamp > s.authentication_token_expiry {
        return respond_err("invalid_grant", "Invalid code");
    }

    // Respond
    let identity = match s.get_identity(&state.identity) {
        Some(i) => i,
        None => {
            return respond_err(
                "invalid_grant",
                "Internal problem with code, please try again.",
            )
        }
    };
    let subject = generate_subject(identity.as_ref(), &state.redirect_uri);
    let access_token = generate_access_token(s, &state.scope, &state.identity, &subject);
    json_response()
        .status(StatusCode::OK)
        .body(Body::from(
            serde_json::to_string(&OIDTokenResponse {
                access_token: &access_token,
                expires_in: s.access_token_expiry,
                scope: &state.scope,
                token_type: "Bearer".to_string(),
                id_token: generate_id_token(
                    s,
                    &context,
                    &Utc.timestamp(state.valid_at, 0),
                    &client_id,
                    state.client_nonce.as_ref().map(|v| v.as_str()),
                    Some(access_token.as_str()),
                    identity.as_ref(),
                    &subject,
                ),
            })
            .unwrap(),
        ))
        .unwrap()
}
