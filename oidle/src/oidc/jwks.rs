use crate::common::{b64e, json_response, ReqContext, ServerInner};
use common_shared::serde::Serialize;
use common_shared::serde_json;
use common_shared::serde_with_macros::skip_serializing_none;
use http::request::Parts;
use hyper::{Body, Chunk, Response, StatusCode};

pub(crate) fn process(
    s: &ServerInner,
    _context: ReqContext,
    _req: Parts,
    _body_bytes: Chunk,
) -> Response<Body> {
    #[skip_serializing_none]
    #[derive(Serialize)]
    struct JWKResponse<'a> {
        keys: Vec<JWKRSAResponse<'a>>,
    }

    #[skip_serializing_none]
    #[derive(Serialize)]
    struct JWKRSAResponse<'a> {
        #[serde(rename = "use")]
        use_: &'a str,
        kid: &'a str,
        kty: &'a str,
        e: &'a str,
        n: &'a str,
        alg: &'a str,
    }

    json_response()
        .status(StatusCode::OK)
        .body(Body::from(
            serde_json::to_string(&JWKResponse {
                keys: vec![JWKRSAResponse {
                    use_: "sig",
                    kid: &s.jwt_pub_key_hash,
                    kty: "RSA",
                    e: &b64e(&s.jwt_pub_key.e),
                    n: &b64e(&s.jwt_pub_key.n),
                    alg: "RS256",
                }],
            })
            .unwrap(),
        ))
        .unwrap()
}
