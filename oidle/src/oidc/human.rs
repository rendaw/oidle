use crate::authenticate as ax;
use crate::common::{basic_response, show_human_message, ReqContext, ServerInner};
use crate::err_do;
use crate::form_parse;
use crate::oidc::common::{generate_access_token, generate_id_token, AuthenticationState};
use crate::oidc::expose_fixed::generate_subject;
use chrono::Utc;
use enum_dispatch::enum_dispatch;
use http::header::LOCATION;
use http::request::Parts;
use hyper::{Body, Chunk, Response, StatusCode};
use std::borrow::Cow;
use std::borrow::Cow::{Borrowed, Owned};
use std::str::FromStr;
use url::Url;

pub(crate) fn process(
    s: &ServerInner,
    context: ReqContext,
    req: Parts,
    body_bytes: Chunk,
) -> Response<Body> {
    let query_bytes = req.uri.query().map(|v| v.as_bytes()).unwrap_or(&[]);
    form_parse!(
        &[query_bytes],
        (b"state", redirect_state),
        (b"response_type", response_type),
        (b"client_id", client_id),
        (b"nonce", client_nonce),
        (b"redirect_uri", redirect_uri),
        (b"scope", scope),
        (b"prompt", prompt),
        (b"max_age", max_age),
    );
    // Validate input
    let scope = scope.unwrap_or(Borrowed("openid"));

    #[enum_dispatch]
    trait RespondClientOIDC {
        fn normal(&self, args: &[(&str, Cow<str>)]) -> Response<Body>;
        fn error(&self, name: &str, desc: &str) -> Response<Body>;
    }

    struct RespondClientRedirectURI<'a> {
        redirect_uri: &'a Url,
        redirect_state: Option<Cow<'a, str>>,
    }
    impl<'a> RespondClientOIDC for RespondClientRedirectURI<'a> {
        fn normal(&self, args: &[(&str, Cow<str>)]) -> Response<Body> {
            let mut url = self.redirect_uri.clone();
            for v in &self.redirect_state {
                url.query_pairs_mut().append_pair("state", v.as_ref());
            }
            url.query_pairs_mut().extend_pairs(args);
            basic_response()
                .status(StatusCode::FOUND)
                .header(LOCATION, url.to_string())
                .body(Body::empty())
                .unwrap()
        }

        fn error(&self, name: &str, desc: &str) -> Response<Body> {
            self.normal(&[
                ("error", Borrowed(name)),
                ("error_description", Borrowed(desc)),
            ])
        }
    }

    struct RespondClientFallback<'a> {
        s: &'a ServerInner,
    }
    impl<'a> RespondClientOIDC for RespondClientFallback<'a> {
        fn normal(&self, args: &[(&str, Cow<str>)]) -> Response<Body> {
            show_human_message(
                self.s,
                &format!(
                    "Code: {}",
                    args.iter().find(|v| v.0.eq("id_token")).unwrap().1
                ),
                "",
            )
        }

        fn error(&self, _: &str, desc: &str) -> Response<Body> {
            show_human_message(self.s, desc, "msg_err")
        }
    }

    #[enum_dispatch(RespondClientOIDC)]
    enum RespondClientImpls<'a> {
        A(RespondClientRedirectURI<'a>),
        B(RespondClientFallback<'a>),
    }

    let parsed_redirect_uri: Option<Url>;
    let (client_realm, respond_client): (_, RespondClientImpls) = match &redirect_uri {
        Some(v) => {
            parsed_redirect_uri = Some(err_do!(
                Url::parse(v).ok().ok_or(()),
                return show_human_message(s, "Invalid redirect_uri", "msg_err")
            ));
            let client_realm = err_do!(
                parsed_redirect_uri.as_ref().unwrap().host().ok_or(()),
                return show_human_message(s, "Invalid redirect_uri", "msg_err")
            )
            .to_string();

            (
                Some(client_realm),
                RespondClientRedirectURI {
                    redirect_uri: parsed_redirect_uri.as_ref().unwrap(),
                    redirect_state: redirect_state.clone(),
                }
                .into(),
            )
        }
        None => {
            parsed_redirect_uri = None;
            (None, RespondClientFallback { s: s }.into())
        }
    };

    let response_type = match response_type {
        Some(v) => v,
        None => {
            return respond_client.error("invalid_request", "Missing response_type");
        }
    };
    let (response_type_code, response_type_id_token, response_type_token) = {
        let mut code = false;
        let mut id_token = false;
        let mut token = false;
        for e in response_type.split(" ") {
            match e {
                "code" => {
                    code = true;
                    if parsed_redirect_uri.is_none() {
                        return respond_client.error("invalid_request", "Missing redirect_uri");
                    }
                }
                "id_token" => {
                    id_token = true;
                    if client_id.is_none() {
                        return respond_client.error("invalid_request", "Missing client_id");
                    }
                }
                "token" => token = true,
                _ => (),
            }
        }
        (code, id_token, token)
    };
    if !response_type_code && !response_type_id_token {
        return respond_client.error(
            "invalid_request",
            "response_type must at least specify code or id_token",
        );
    }
    let max_age = match max_age.map(|v| i64::from_str(v.as_ref())) {
        Some(Ok(v)) => Some(v),
        None => None,
        Some(Err(_)) => {
            return respond_client.error("invalid_request", "Cannot parse max_time argument");
        }
    };

    // Do authentication
    let now = Utc::now();
    let mut self_url = Url::parse(&context.full_url).unwrap();
    self_url.query_pairs_mut().clear();
    for (k, v) in &[
        ("state", redirect_state),
        ("response_type", Some(response_type)),
        ("client_id", client_id.clone()),
        ("nonce", client_nonce.clone()),
        ("scope", Some(scope.clone())),
        ("redirect_uri", redirect_uri),
    ] {
        for v in v {
            self_url.query_pairs_mut().append_pair(k, v.as_ref());
        }
    }
    let done = match ax::progress_authentication(
        s,
        &req,
        &body_bytes,
        now,
        client_realm.as_ref().map(|v| Borrowed(v.as_str())),
        self_url,
        prompt,
        max_age,
    ) {
        Ok(ax::Resp::Continue(r)) => return r,
        Err(ax::Error::Client(r)) => return respond_client.error(&r.code, &r.desc),
        Ok(ax::Resp::Done(d)) => d,
    };

    // Send response
    let mut response_values: Vec<(&str, Cow<str>)> = vec![("scope", scope.clone())];
    if response_type_code {
        response_values.push((
            "code",
            Owned(
                AuthenticationState {
                    timestamp: now.timestamp(),
                    redirect_uri: parsed_redirect_uri.as_ref().unwrap().to_string(),
                    scope: scope.to_string(),
                    identity: done.identity_key.clone(),
                    client_nonce: client_nonce.as_ref().map(|v| v.to_string()),
                    valid_at: done.session_valid_at.timestamp(),
                }
                .encode(s),
            ),
        ));
    }
    if response_type_id_token || response_type_token {
        let subject = generate_subject(
            done.identity.as_ref(),
            client_realm.as_ref().map(|v| v.as_str()).unwrap_or(""),
        );
        let mut access_token = None;
        if response_type_token {
            let t = generate_access_token(s, scope.as_ref(), &done.identity_key, &subject);
            access_token = Some(t.clone());
            response_values.push(("access_token", t.into()));
            response_values.push(("expires_in", s.access_token_expiry.to_string().into()));
            response_values.push(("scope", scope.into()));
            response_values.push(("token_type", "Bearer".into()));
        }
        if response_type_id_token {
            response_values.push((
                "id_token",
                generate_id_token(
                    s,
                    &context,
                    &done.session_valid_at,
                    &client_id.as_ref().unwrap(),
                    client_nonce.as_ref().map(|v| v.as_ref()),
                    access_token.as_ref().map(|v| v.as_str()),
                    done.identity.as_ref(),
                    &subject,
                )
                .into(),
            ));
        }
    }
    respond_client.normal(response_values.as_slice())
}
