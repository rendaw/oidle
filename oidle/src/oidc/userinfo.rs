extern crate idna;

use crate::common::{b64d, basic_response, json_response, ReqContext, ServerInner};
use crate::fixed::decrypt;
use crate::form_parse;
use crate::oidc::common::{clone_claims, AccessToken};
use chrono::{TimeZone, Utc};
use common_shared::serde_json;
use http::header::{AUTHORIZATION, WWW_AUTHENTICATE};
use http::request::Parts;
use hyper::{Body, Chunk, Response, StatusCode};

pub(crate) fn process(
    s: &ServerInner,
    _context: ReqContext,
    req: Parts,
    body_bytes: Chunk,
) -> Response<Body> {
    match || -> Result<Response<Body>, &'static str> {
        form_parse!(&[&body_bytes], (b"access_token", raw_token));
        loop {
            if raw_token.is_none() {
                const BEARER: &str = "Bearer ";
                let authorization = match req.headers.get(AUTHORIZATION) {
                    Some(v) => v,
                    None => break,
                };
                let authorization = authorization
                    .to_str()
                    .map_err(|_| "Authorization header unparsable")?;
                if !authorization.starts_with(BEARER) {
                    return Err("Only Bearer type authorization header allowed");
                }
                raw_token = Some(authorization[BEARER.len()..].to_string().into());
            }
            break;
        }
        let raw_token = match raw_token {
            Some(v) => v,
            None => return Err("Missing token in header/body!"),
        };
        let access_token: AccessToken<String> = serde_json::from_slice(
            decrypt(
                s,
                &b64d(&raw_token.as_bytes()).map_err(|_| "Token corrupt")?,
            )
            .ok_or_else(|| "Invalid token")?
            .as_slice(),
        )
        .map_err(|_| "Token corrupt")?;
        if Utc::now()
            .signed_duration_since(Utc.timestamp(access_token.timestamp, 0))
            .num_seconds()
            > s.access_token_expiry
        {
            return Err("Token expired");
        }
        Ok(json_response()
            .status(StatusCode::OK)
            .body(Body::from(
                serde_json::to_string(&clone_claims(
                    &access_token.subject,
                    &s.get_identity(&access_token.identity_hash)
                        .ok_or("Outdated information in token, please try again")?
                        .claims,
                ))
                .unwrap(),
            ))
            .unwrap())
    }() {
        Ok(r) => r,
        Err(e) => basic_response()
            .status(StatusCode::UNAUTHORIZED)
            .header(
                WWW_AUTHENTICATE,
                format!(
                    "error=\"invalid_token\", error_description=\"{}\"",
                    idna::punycode::encode_str(&e.to_string()).unwrap()
                ),
            )
            .body(Body::empty())
            .unwrap(),
    }
}
