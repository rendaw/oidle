// FIXED STRUCTURES
// These may be stored in remote databases, so the serialized representation should not change

use crate::common::ConfigurationIdentity;
use crate::fixed::{rand_str, seed_rand};

// Subject IDs
// ____
// Requirements:
// * Unique per site
// * Not predictable, can't be correlated to other ids when pairwise identity on
// * Only used on client side, never returned to us (where we need it)
//
// Implementation
// * Random string using identity + client host as a seed
pub(crate) fn generate_subject(identity: &ConfigurationIdentity, redirect_host: &str) -> String {
    if identity.pairwise_subject {
        let mut subject_rand = seed_rand(&[identity.sub.as_bytes(), &redirect_host.as_bytes()]);
        rand_str(&mut subject_rand, 16)
    } else {
        identity.sub.clone()
    }
}
