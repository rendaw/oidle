use crate::common::ServerInner;
use crate::fixed::{b64d, b64e};
use common_shared::serde_json;
use serde::{Deserialize, Serialize};

// Client id + secret
// ____
// * Used to offload client info storage
// * Client must not be able to modify the data
// * No secret information contained within it, so encryption not necessary
//
// Implementation
// * Client id is json
// * Client secret is detached signature
#[derive(Serialize, Deserialize)]
pub(crate) struct OIDClientID {
    #[serde(rename = "r")]
    pub redirect_uris: Vec<String>,
    #[serde(rename = "s")]
    pub sector_identifier_uri: Option<String>,
}

impl OIDClientID {
    pub(crate) fn decode(
        s: &ServerInner,
        client_id: &str,
        client_secret: &str,
    ) -> Option<OIDClientID> {
        b64d(client_id)
            .ok()
            .and_then(|client_id| {
                b64d(client_secret)
                    .ok()
                    .and_then(|s| sodiumoxide::crypto::sign::ed25519::Signature::from_slice(&s))
                    .map(|client_secret| (client_id, client_secret))
            })
            .filter(|(client_id, client_secret)| {
                sodiumoxide::crypto::sign::ed25519::verify_detached(
                    client_secret,
                    client_id,
                    &s.sign_pub_key,
                )
            })
            .and_then(|(client_id, _)| serde_json::from_slice(&client_id).ok())
    }

    pub(crate) fn encode(&self, s: &ServerInner) -> (String, String) {
        let client_id = serde_json::to_string(self).unwrap();
        (
            b64e(client_id.as_bytes()),
            b64e(
                sodiumoxide::crypto::sign::ed25519::sign_detached(
                    client_id.as_bytes(),
                    &s.sign_priv_key,
                )
                .0
                .as_ref(),
            ),
        )
    }
}
