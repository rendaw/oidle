use crate::common::{b64d, b64e, ConfigurationIdentity, ReqContext, ServerInner};
use crate::fixed::{decrypt, encrypt, sha256};
use chrono::{DateTime, Utc};
use common_shared::serde::{Deserialize, Serialize};
use common_shared::serde_json;
use common_shared::serde_json::Value;
use common_shared::serde_with_macros::skip_serializing_none;
use jsonwebtoken::{Algorithm, Header};
use std::collections::HashMap;

#[skip_serializing_none]
#[derive(Serialize)]
pub(crate) struct JWTOIDClaims<'a> {
    iss: &'a str,
    sub: &'a str,
    aud: &'a str,
    exp: i64,
    iat: i64,
    nonce: Option<&'a str>,
    at_hash: Option<&'a str>,
    auth_time: i64,
    #[serde(flatten)]
    profile: HashMap<String, Value>,
}

#[skip_serializing_none]
#[derive(Serialize)]
pub(crate) struct OIDTokenResponse<'a> {
    pub access_token: &'a String,
    pub expires_in: i64,
    pub scope: &'a String,
    pub token_type: String,
    pub id_token: String,
}

#[derive(Serialize, Deserialize)]
pub(crate) struct AuthenticationState {
    #[serde(rename = "t")]
    pub timestamp: i64,
    #[serde(rename = "r")]
    pub redirect_uri: String,
    #[serde(rename = "s")]
    pub scope: String,
    #[serde(rename = "i")]
    pub identity: String,
    #[serde(rename = "n")]
    pub client_nonce: Option<String>,
    #[serde(rename = "v")]
    pub valid_at: i64,
}

impl AuthenticationState {
    pub(crate) fn decode(s: &ServerInner, code: &str) -> Option<AuthenticationState> {
        b64d(code)
            .ok()
            .and_then(|v| decrypt(s, &v))
            .and_then(|v| serde_json::from_slice(&v).ok())
    }

    pub(crate) fn encode(&self, s: &ServerInner) -> String {
        b64e(&encrypt(s, serde_json::to_string(self).unwrap().as_bytes()))
    }
}

pub(crate) fn generate_id_token(
    s: &ServerInner,
    context: &ReqContext,
    valid_at: &DateTime<Utc>,
    client_id: &str,
    client_nonce: Option<&str>,
    access_token: Option<&str>,
    identity: &ConfigurationIdentity,
    subject: &str,
) -> String {
    let now = Utc::now().timestamp();
    let at_hash = access_token
        .as_ref()
        .map(|t| b64e(&sha256(t.as_bytes())[0..16]));
    jsonwebtoken::encode(
        &Header {
            typ: Some("JWT".to_string()),
            alg: Algorithm::RS256,
            cty: None,
            jku: None,
            kid: Some(s.jwt_pub_key_hash.clone()),
            x5u: None,
            x5t: None,
        },
        &JWTOIDClaims {
            iss: &context.issuer,
            sub: subject,
            aud: &client_id,
            exp: now + s.id_token_verification_expiry,
            iat: now,
            nonce: client_nonce,
            auth_time: valid_at.timestamp(),
            at_hash: at_hash.as_ref().map(|v| v.as_str()),
            profile: clone_claims(&subject, &identity.claims),
        },
        s.jwt_pubpriv_key.as_slice(),
    )
    .unwrap()
}

pub(crate) fn generate_access_token(
    s: &ServerInner,
    scope: &str,
    identity_key: &str,
    subject: &str,
) -> String {
    b64e(&encrypt(
        s,
        serde_json::to_string(&AccessToken {
            timestamp: Utc::now().timestamp(),
            scope: scope,
            identity_hash: identity_key,
            subject: subject,
        })
        .unwrap()
        .into_bytes()
        .as_slice(),
    ))
}

pub fn clone_claims(sub: &str, claims: &HashMap<String, Value>) -> HashMap<String, Value> {
    let mut out = HashMap::new();
    for (k, v) in claims {
        out.insert(
            k.clone(),
            match v {
                Value::String(v) => Value::String(v.replace("{}", sub)),
                x => x.clone(),
            },
        );
    }
    out.insert("sub".to_string(), Value::String(sub.to_string()));
    out
}

#[derive(Serialize, Deserialize)]
pub struct AccessToken<T> {
    #[serde(rename = "t")]
    pub timestamp: i64,
    #[serde(rename = "s")]
    pub scope: T,
    #[serde(rename = "i")]
    pub identity_hash: T,
    #[serde(rename = "u")]
    pub subject: T,
}

pub struct JWTPublicKey {
    pub n: Vec<u8>,
    pub e: Vec<u8>,
}
