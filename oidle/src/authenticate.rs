use crate::common::VecExtract;
use crate::common::{
    b64d, b64e, basic_response, html_response, Association, ConfigurationIdentity, Fido2Assert,
    Fido2Error, ServerInner, Session, COSE_ES256, FIDO_ERR_INVALID_SIG,
};
use crate::fixed::sha256;
use crate::form_parse;
use chrono::{DateTime, TimeZone, Utc};
use common_shared::rand::Rng;
use common_shared::serde::export::Formatter;
use cookie::Cookie;
use cookie::SameSite::Lax;
use http::header::LOCATION;
use http::request::Parts;
use hyper::http::header::SET_COOKIE;
use hyper::{Body, Chunk, Response, StatusCode};
use log::debug;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use std::borrow::Cow::Borrowed;
use std::collections::HashMap;
use std::sync::{Arc, Mutex, TryLockError};
use url::Url;

pub trait VecContainsStr {
    fn contains_str(&self, v: &str) -> bool;
}

impl VecContainsStr for Vec<String> {
    fn contains_str(&self, v: &str) -> bool {
        self.iter().any(|e| e == v)
    }
}

const SESSION_COOKIE: &str = "oidle_session";

#[derive(Serialize, Deserialize)]
struct WebAuthnClientDataJSON<T> {
    #[serde(rename = "type")]
    pub type_: T,
    pub challenge: T,
    pub origin: T,
}

pub(crate) struct Done {
    pub(crate) identity_key: String,
    pub(crate) identity: Arc<ConfigurationIdentity>,
    pub(crate) session_valid_at: DateTime<Utc>,
}

pub(crate) struct ClientErr {
    pub(crate) desc: Cow<'static, str>,
    pub(crate) code: Cow<'static, str>,
}

pub(crate) enum Resp {
    Continue(Response<Body>),
    Done(Done),
}

pub(crate) enum Error {
    Client(ClientErr),
}

pub(crate) fn progress_authentication(
    s: &ServerInner,
    req: &Parts,
    body_bytes: &Chunk,
    now: DateTime<Utc>,
    realm: Option<Cow<str>>,
    self_url: Url,
    prompt: Option<Cow<str>>,
    max_age: Option<i64>,
) -> Result<Resp, Error> {
    let dest_host = realm.unwrap_or(Borrowed("(Unknown)"));

    let post_redirect_ret = || {
        Ok(Resp::Continue(
            basic_response()
                .status(StatusCode::SEE_OTHER)
                .header(LOCATION, self_url.as_str())
                .body(Body::empty())
                .unwrap(),
        ))
    };
    let human_redirect_prompt = |r: &[&str]| {
        let mut target = self_url.clone();
        if !r.is_empty() {
            target.query_pairs_mut().append_pair("prompt", &r.join(" "));
        }
        Ok(Resp::Continue(
            basic_response()
                .status(StatusCode::FOUND)
                .header(LOCATION, target.to_string())
                .body(Body::empty())
                .unwrap(),
        ))
    };

    // Get the session
    let session_id = req
        .headers
        .get("cookie")
        .and_then(|v| {
            (match v.to_str() {
                Err(e) => {
                    debug!("Corrupt cookie in request: {}", e);
                    None
                }
                Ok(v) => Some(v),
            })
            .and_then(|v| {
                v.split(";")
                    .filter_map(|raw| match Cookie::parse(raw) {
                        Err(_) => None,
                        Ok(v) => {
                            if v.name().eq(SESSION_COOKIE) {
                                Some(v.value().to_string())
                            } else {
                                None
                            }
                        }
                    })
                    .next()
            })
        })
        .unwrap_or_else(|| b64e(&common_shared::rand::thread_rng().gen::<[u8; 32]>()));
    let session = {
        let mut sessions = s.sessions.lock().unwrap();
        sessions.retain(|(_id, session)| {
            let session = session.clone();
            let session = match session.try_lock() {
                Ok(s) => Ok(s),
                Err(TryLockError::WouldBlock) => return true,
                Err(e) => Err(e),
            }
            .unwrap();
            let max_age = if session.valid {
                s.session_valid_expiry
            } else {
                s.session_incomplete_expiry
            };
            if now.signed_duration_since(session.created).num_seconds() > max_age {
                return false;
            }
            return true;
        });
        sessions
            .iter()
            .find(|(id, _session)| id.eq(&session_id))
            .map(|(_id, session)| session.clone())
    }
    .unwrap_or_else(|| {
        let mut sessions = s.sessions.lock().unwrap();
        let session = Arc::new(Mutex::new(Session {
            created: now,
            password: false,
            webauthn: false,
            webauthn_challenge: None,
            associations: HashMap::new(),
            valid: false,
            valid_at: Utc.timestamp(0, 0),
        }));
        sessions.push((session_id.clone(), session.clone()));
        session
    });
    let mut session = session.lock().unwrap();

    let mut prompt: Vec<&str> = prompt
        .as_ref()
        .map(|v| v.split(" ").collect())
        .unwrap_or_else(|| vec![]);
    let mut reset_prompt = false;
    if prompt.extract_one(|v| v.eq(&"login")).is_some()
        || (session.valid
            && match max_age {
                Some(v) => now.signed_duration_since(session.valid_at).num_seconds() > v,
                _ => false,
            })
    {
        session.password = false;
        session.webauthn = false;
        session.webauthn_challenge = None;
        session.valid = false;
        reset_prompt = true;
    }
    if prompt.extract_one(|v| v.eq(&"consent")).is_some() {
        session
            .associations
            .entry(
                dest_host.to_string(), /* https://github.com/rust-lang/rust/issues/51604 */
            )
            .and_modify(|v| v.confirmed = false);
        reset_prompt = true;
    }
    if reset_prompt {
        return human_redirect_prompt(prompt.as_slice());
    }

    // Process user/password or show user/password screen
    if s.password.is_some() && !session.password {
        if prompt.contains(&"none") {
            return Err(Error::Client(ClientErr {
                code: "login_requred".into(),
                desc: "Cannot abide prompt=none, user must log in".into(),
            }));
        }

        form_parse!(&[&body_bytes], (b"user", user), (b"pass", pass));

        #[derive(Serialize)]
        struct TemplateArgs<'a> {
            action: &'a str,
            dest_host: Cow<'a, str>,
            user: Option<Cow<'a, str>>,
            error: Option<&'static str>,
        }
        let template_ret = |a: TemplateArgs| {
            Ok(Resp::Continue(
                html_response()
                    .status(StatusCode::OK)
                    .header(
                        SET_COOKIE,
                        Cookie::build(SESSION_COOKIE, session_id.clone())
                            .http_only(true)
                            .same_site(Lax)
                            .expires(time::now() + time::Duration::seconds(s.session_valid_expiry))
                            .finish()
                            .to_string(),
                    )
                    .body(Body::from(s.handlebars.render("login.html", &a).unwrap()))
                    .unwrap(),
            ))
        };

        if user.is_some() && pass.is_none() {
            return template_ret(TemplateArgs {
                action: self_url.as_str(),
                dest_host,
                user: user,
                error: Some(&"Please enter your password"),
            });
        } else if user.is_none() && pass.is_some() {
            return template_ret(TemplateArgs {
                action: self_url.as_str(),
                dest_host,
                user: None,
                error: Some(&"Please enter your username"),
            });
        } else if user.is_none() && pass.is_none() {
            return template_ret(TemplateArgs {
                action: self_url.as_str(),
                dest_host,
                user: None,
                error: None,
            });
        }
        if user
            .as_ref()
            .and_then(|v| {
                if s.username == v.as_ref() {
                    Some(())
                } else {
                    None
                }
            })
            .and_then(|_| {
                pass.and_then(|v| {
                    if sodiumoxide::crypto::pwhash::pwhash_verify(
                        s.password.as_ref().unwrap(),
                        v.as_bytes(),
                    ) {
                        Some(())
                    } else {
                        None
                    }
                })
            })
            .is_none()
        {
            return template_ret(TemplateArgs {
                action: self_url.as_str(),
                dest_host,
                user: user,
                error: Some(&"Invalid username or password"),
            });
        } else {
            session.password = true;
            return post_redirect_ret();
        }
    }

    // Process webauthn response or send webauthn challenge
    if !s.webauthn.is_empty() && !session.webauthn {
        if prompt.contains(&"none") {
            return Err(Error::Client(ClientErr {
                code: "login_required".into(),
                desc: "Cannot abide prompt=none, user must log in".into(),
            }));
        }

        let mut error = None;
        if session.webauthn_challenge.is_some() && !body_bytes.is_empty() {
            form_parse!(
                &[&body_bytes],
                (b"authenticatorId", authenticator_id),
                (b"clientDataJSON", client_data_json),
                (b"signature", signature),
                (b"authenticatorData", authenticator_data),
            );
            #[derive(Debug)]
            enum WebauthnError {
                S(&'static str),
                Fido2(Fido2Error),
            }

            impl std::fmt::Display for WebauthnError {
                fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
                    std::fmt::Debug::fmt(self, f)
                }
            }

            impl From<&'static str> for WebauthnError {
                fn from(e: &'static str) -> Self {
                    WebauthnError::S(e)
                }
            }

            impl From<Fido2Error> for WebauthnError {
                fn from(e: Fido2Error) -> Self {
                    WebauthnError::Fido2(e)
                }
            }

            match || -> Result<bool, WebauthnError> {
                let client_data_json = b64d(
                    client_data_json
                        .ok_or_else(|| "Missing clientDataJSON")?
                        .as_bytes(),
                )
                .map_err(|_| "Error parsing clientDataJSON base64")?;
                let authenticator_id: Cow<'_, str> =
                    authenticator_id.ok_or_else(|| "Missing authenticator id")?;
                let signature: Vec<u8> =
                    signature
                        .ok_or_else(|| "Missing signature")
                        .and_then(|signature| {
                            b64d(signature.as_bytes()).map_err(|_| "Error parsing signature base64")
                        })?;
                let authenticator_data: Vec<u8> = authenticator_data
                    .ok_or_else(|| "Missing authenticatorData")
                    .and_then(|authenticator_data| {
                        b64d(authenticator_data.as_bytes())
                            .map_err(|_| "Error parsing authenticatorData base64")
                    })?;
                let client_data: WebAuthnClientDataJSON<String> =
                    common_shared::serde_json::from_slice(client_data_json.as_slice())
                        .map_err(|_| "Error parsing clientDataJSON json")?;
                if b64d(&client_data.challenge)
                    .map_err(|_| "Error parsing clientDataJSON challenge base64")?
                    .ne(&session.webauthn_challenge.take().unwrap())
                {
                    return Ok(false);
                }

                if Url::parse(&client_data.origin)
                    .map_err(|_| "Error parsing clientDataJSON origin")?
                    .domain()
                    .ok_or_else(|| "Invalid origin in client data; missing domain")?
                    .ne(&s.domain)
                {
                    return Ok(false);
                }

                if client_data.type_.ne("webauthn.get") {
                    return Ok(false);
                }

                let key = &s
                    .webauthn
                    .iter()
                    .find(|(id, _)| id.eq(&authenticator_id))
                    .ok_or_else(|| "Authenticator not in registered list")?
                    .1;

                let assertion = Fido2Assert::new().unwrap();
                assertion.set_rp(&s.domain).unwrap();
                assertion
                    .set_clientdata_hash(&sha256(client_data_json.as_slice()))
                    .unwrap();
                assertion.set_authdata_raw(&authenticator_data).unwrap();
                assertion.set_sig(signature.as_slice()).unwrap();
                match assertion.verify(COSE_ES256, key.as_slice()) {
                    Ok(()) => Ok(true),
                    Err(Fido2Error::Raw(FIDO_ERR_INVALID_SIG)) => Ok(false),
                    Err(e) => Err(e.into()),
                }
            }() {
                Ok(true) => {
                    session.webauthn = true;
                    return post_redirect_ret();
                }
                Ok(false) => {
                    error = Some("Authentication failed, please check everything and try again.");
                }
                Err(e) => {
                    debug!("Internal error verifying WebAuthn signature: {}", e);
                    error = Some("Error processing response, please complete the prompt again.");
                }
            };
        }

        session.webauthn_challenge = Some(sodiumoxide::randombytes::randombytes(32));
        #[derive(Serialize)]
        struct TemplateSubArgs<'a> {
            ids: Vec<&'a str>,
            domain: &'a str,
            challenge: Option<String>,
        }
        #[derive(Serialize)]
        struct TemplateArgs<'a> {
            action: &'a str,
            dest_host: Cow<'a, str>,
            error: Option<&'static str>,
            data: String,
        }
        return Ok(Resp::Continue(
            html_response()
                .status(StatusCode::OK)
                .body(Body::from(
                    s.handlebars
                        .render(
                            "webauthn.html",
                            &TemplateArgs {
                                action: self_url.as_str(),
                                dest_host,
                                data: common_shared::serde_json::to_string(&TemplateSubArgs {
                                    ids: s.webauthn.iter().map(|(id, _)| id.as_str()).collect(),
                                    domain: &s.domain,
                                    challenge: session.webauthn_challenge.as_ref().map(|v| b64e(v)),
                                })
                                .unwrap(),
                                error,
                            },
                        )
                        .unwrap(),
                ))
                .unwrap(),
        ));
    }

    // Login completed
    session.valid = true;
    session.valid_at = now;

    // Fill in assocation
    let association = session
        .associations
        .entry(
            dest_host.to_string(), /* https://github.com/rust-lang/rust/issues/51604 */
        )
        .or_insert_with(|| Association {
            identity: None,
            confirmed: false,
        });

    if prompt.extract_one(|v| v.eq(&"select_account")).is_some() {
        association.identity = None;
        return human_redirect_prompt(prompt.as_slice());
    }

    // Process identity response or show identity page
    if association.identity.is_none() {
        if prompt.contains(&"none") {
            return Err(Error::Client(ClientErr {
                code: "account_selection_required".into(),
                desc: "Cannot abide prompt=none, user must complete account selection".into(),
            }));
        }

        if s.identities.len() == 1 {
            association.identity = s.identities.first().map(|(k, v)| (k.clone(), v.clone()));
        } else {
            loop {
                form_parse!(&[&body_bytes], (b"identity", identity));
                let mut error = None;
                if identity.is_some() {
                    let identity = identity.unwrap();
                    match s.get_identity(&identity) {
                        Some(p) => {
                            association.identity = Some((identity.to_string(), p));
                            break;
                        }
                        None => {
                            error =
                                Some(format!("Internal error: no identity with id {}", identity));
                        }
                    };
                }
                #[derive(Serialize)]
                struct TemplateArgs<'a> {
                    action: &'a str,
                    dest_host: Cow<'a, str>,
                    error: Option<String>,
                    identities: String,
                }
                return Ok(Resp::Continue(
                    html_response()
                        .status(StatusCode::OK)
                        .body(Body::from(
                            s.handlebars
                                .render(
                                    "identity.html",
                                    &TemplateArgs {
                                        action: self_url.as_str(),
                                        dest_host,
                                        error,
                                        identities: common_shared::serde_json::to_string::<
                                            Vec<(&String, &ConfigurationIdentity)>,
                                        >(
                                            &s.identities
                                                .iter()
                                                .map(|(key, value)| (key, value.as_ref()))
                                                .collect(),
                                        )
                                        .unwrap(),
                                    },
                                )
                                .unwrap(),
                        ))
                        .unwrap(),
                ));
            }
        }
    }
    let (identity_key, identity) = {
        let r = association.identity.as_ref().unwrap();
        (r.0.clone(), r.1.clone())
    };

    // Confirm access
    if s.confirm && !association.confirmed {
        loop {
            form_parse!(&[&body_bytes], (b"confirm", confirm));
            let confirm = confirm.map(|v| v.eq("1")).unwrap_or(false);
            if confirm {
                association.confirmed = true;
                break;
            }
            #[derive(Serialize)]
            struct TemplateArgs<'a> {
                action: &'a str,
                dest_host: Cow<'a, str>,
            }
            return Ok(Resp::Continue(
                html_response()
                    .status(StatusCode::OK)
                    .body(Body::from(
                        s.handlebars
                            .render(
                                "confirm.html",
                                &TemplateArgs {
                                    action: self_url.as_str(),
                                    dest_host,
                                },
                            )
                            .unwrap(),
                    ))
                    .unwrap(),
            ));
        }
    }

    Ok(Resp::Done(Done {
        identity_key,
        identity,
        session_valid_at: session.valid_at.clone(),
    }))
}
