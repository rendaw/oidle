extern crate chrono;
extern crate common as common_shared;
extern crate http;
extern crate hyper;
extern crate jsonwebtoken;
extern crate ring;
extern crate serde;
extern crate time;
use common_shared::{b64e, sha2};
use serde::Serialize;

use std::net::SocketAddr;
use std::str::FromStr;
mod authenticate;
mod common;
mod fixed;
mod ldap;
mod oid2;
mod oidc;

use crate::common::{
    b64d, basic_response, fido_init, html_response, join, repr, AwfulRouter, Configuration,
    ReqContext, ServerInner, DEFAULT_ACCESS_TOKEN_EXPIRY, DEFAULT_AUTHENTICATION_TOKEN_EXPIRY,
    DEFAULT_ID_TOKEN_VERIFICATION_EXPIRY, DEFAULT_LISTEN, DEFAULT_LISTEN_LDAP,
    DEFAULT_SESSION_INCOMPLETE_EXPIRY, DEFAULT_SESSION_VALID_EXPIRY, JWKS_PATH, OID2_PATH,
    OIDC_DISCOVERY_PATH, OIDC_HUMAN_PATH, OIDC_REGISTRATION_PATH, OIDC_TOKEN_PATH,
    OIDC_USERINFO_PATH,
};
use crate::fixed::sha256;
use crate::oidc::common::JWTPublicKey;
use handlebars::Handlebars;
use hmac::digest::Digest;
use http::header::{
    ACCESS_CONTROL_ALLOW_HEADERS, ACCESS_CONTROL_ALLOW_METHODS, ACCESS_CONTROL_REQUEST_HEADERS,
    ACCESS_CONTROL_REQUEST_METHOD,
};
use http::request::Parts;
use http::uri::Authority;
use http::uri::Scheme;
use http::Method;
use hyper::rt::{Future, Stream};
use hyper::service::{NewService, Service};
use hyper::{Body, Chunk, Request, Response, Server as HyperServer, StatusCode};
use log::{debug, error, trace};
use ring::io::der;
use ring::signature::KeyPair;
use simplelog::{ConfigBuilder, LevelFilter, SimpleLogger};
use sodiumoxide::crypto::pwhash::HashedPassword;
use std::borrow::Borrow;
use std::cmp::max;
use std::collections::HashSet;
use std::sync::{Arc, Mutex};
use tokio::net::TcpListener;
use tokio::prelude::future;
use tokio::runtime;
use tokio_timer::clock::Clock;
use untrusted::Input;

pub(crate) type HttpFut = Box<dyn Future<Item = Response<Body>, Error = hyper::Error> + Send>;

#[derive(Clone)]
pub(crate) struct Server(Arc<ServerInner>);

impl Server {
    fn new(config: &Configuration) -> Result<Server, String> {
        fido_init();

        let sign_keys = sodiumoxide::crypto::sign::keypair_from_seed(
            &sodiumoxide::crypto::sign::Seed::from_slice(config.general_key.as_slice()).unwrap(),
        );

        let mut handlebars = Handlebars::new();
        for p in &[
            "root.html",
            "message.html",
            "login.html",
            "webauthn.html",
            "identity.html",
            "confirm.html",
        ] {
            handlebars
                .register_template_file(&p, "templates/".to_string() + &p)
                .map_err(|e| format!("Error loading html template [{}]: {}", p, e))?;
        }

        let jwt_pub_key = Input::from(
            ring::signature::RsaKeyPair::from_der(Input::from(
                config.jwt_pubpriv_key_der.as_slice(),
            ))
            .map_err(|e| format!("Error loading jwt_pub_key: {}", e))?
            .public_key()
            .as_ref(),
        )
        .read_all(ring::error::Unspecified, |input| {
            der::nested(
                input,
                der::Tag::Sequence,
                ring::error::Unspecified,
                |input| {
                    Ok(JWTPublicKey {
                        n: der::positive_integer(input)?
                            .big_endian_without_leading_zero()
                            .as_slice_less_safe()
                            .to_vec(),
                        e: der::positive_integer(input)?
                            .big_endian_without_leading_zero()
                            .as_slice_less_safe()
                            .to_vec(),
                    })
                },
            )
        })
        .map_err(|e| format!("Error reading jwt_pub_key DER: {}", e))?;

        let jwt_pub_key_hash = {
            let mut d = sha2::Sha256::default();
            d.input(&jwt_pub_key.n);
            d.input(&jwt_pub_key.e);
            b64e(d.result().as_slice())
        };

        Ok(Server(Arc::new(ServerInner {
            authentication_token_expiry: config
                .authentication_token_expiry
                .unwrap_or(DEFAULT_AUTHENTICATION_TOKEN_EXPIRY),
            access_token_expiry: config
                .access_token_expiry
                .unwrap_or(DEFAULT_ACCESS_TOKEN_EXPIRY),
            id_token_verification_expiry: config
                .id_token_verification_expiry
                .unwrap_or(DEFAULT_ID_TOKEN_VERIFICATION_EXPIRY),
            session_incomplete_expiry: config
                .session_incomplete_expiry
                .unwrap_or(DEFAULT_SESSION_INCOMPLETE_EXPIRY),
            session_valid_expiry: config
                .session_valid_expiry
                .unwrap_or(DEFAULT_SESSION_VALID_EXPIRY),
            force_https: config.force_https,
            clients: config.clients.clone(),
            domain: config.domain.clone(),
            username: config.username.clone(),
            password: config
                .password
                .as_ref()
                .map(|p| HashedPassword::from_slice(p.0.as_slice()).unwrap()),
            confirm: config.confirm,
            identities: config
                .identities
                .iter()
                .map(|e| (b64e(&sha256(e.name.as_bytes())), Arc::new(e.clone())))
                .collect::<Vec<_>>()
                .into_boxed_slice(),
            jwt_pubpriv_key: config.jwt_pubpriv_key_der.clone(),
            jwt_pub_key,
            jwt_pub_key_hash,
            sign_priv_key: sign_keys.1,
            sign_pub_key: sign_keys.0,
            encrypt_key: sodiumoxide::crypto::secretbox::Key::from_slice(&config.general_key)
                .unwrap(),
            general_priv_key: config.general_key.clone(),
            general_priv_key_mask: config.general_key_mask.clone(),
            webauthn: config
                .webauthn
                .iter()
                .map(|(id, key)| (id.clone(), b64d(key).unwrap()))
                .collect(),
            sessions: Mutex::new(vec![]),
            handlebars,
            ldap_attributes: config.ldap_attributes.clone(),
            routes: {
                let out: AwfulRouter<fn(&ServerInner, ReqContext, Parts, Chunk) -> Response<Body>> =
                    AwfulRouter::new(vec![
                        ("/", {
                            fn o(
                                self1: &ServerInner,
                                context: ReqContext,
                                _req: Parts,
                                _body_bytes: Chunk,
                            ) -> Response<Body> {
                                #[derive(Serialize)]
                                struct TemplateArgs<'a> {
                                    oid2: &'a str,
                                }
                                html_response()
                                    .status(StatusCode::OK)
                                    .body(Body::from(
                                        self1
                                            .handlebars
                                            .render(
                                                "root.html",
                                                &TemplateArgs {
                                                    oid2: &context.join(Some(OID2_PATH)),
                                                },
                                            )
                                            .unwrap(),
                                    ))
                                    .unwrap()
                            }
                            (true, o)
                        }),
                        (OIDC_DISCOVERY_PATH, (true, oidc::discovery::process)),
                        (JWKS_PATH, (true, oidc::jwks::process)),
                        (OIDC_REGISTRATION_PATH, (true, oidc::register::process)),
                        (OIDC_HUMAN_PATH, (true, oidc::human::process)),
                        (OIDC_TOKEN_PATH, (true, oidc::token::process)),
                        (OIDC_USERINFO_PATH, (true, oidc::userinfo::process)),
                        (OID2_PATH, (false, oid2::process)),
                    ]);
                out
            },
        })))
    }

    fn process(&self, req: Parts, body_bytes: Chunk) -> Response<Body> {
        match &req.method {
            &Method::OPTIONS => {
                let mut resp = basic_response();
                for (forward, dest) in &[
                    (ACCESS_CONTROL_REQUEST_HEADERS, ACCESS_CONTROL_ALLOW_HEADERS),
                    (ACCESS_CONTROL_REQUEST_METHOD, ACCESS_CONTROL_ALLOW_METHODS),
                ] {
                    req.headers.get(forward).map(|v| {
                        resp.header(dest, v);
                        ()
                    });
                }
                return resp.body(Body::empty()).unwrap();
            }
            _ => {}
        }
        let proto = req
            .headers
            .get("x-forwarded-proto")
            .and_then(|v| {
                match v
                    .to_str()
                    .map_err(|_| ())
                    .and_then(|v| Scheme::from_str(v).map_err(|_| ()))
                {
                    Ok(v) => Some(v),
                    Err(_) => {
                        debug!("Unparsable x-forwarded-proto in request: {:?}", v);
                        None
                    }
                }
            })
            .or_else(|| req.uri.scheme_part().map(|v| v.clone()))
            .unwrap_or_else(|| Scheme::HTTP);
        if self.0.force_https && proto.ne("https") {
            return basic_response()
                .status(StatusCode::BAD_REQUEST)
                .body(Body::from("HTTPS required."))
                .unwrap();
        }
        let host = err_do!(
            req.headers
                .get("x-forwarded-host")
                .or_else(|| req.headers.get("host"))
                .and_then(|v| {
                    match v
                        .to_str()
                        .map_err(|_| ())
                        .and_then(|v| Authority::from_str(v).map_err(|_| ()))
                    {
                        Ok(v) => Some(v),
                        Err(_) => {
                            debug!(
                                "Unparsable x-forwarded-host or host header in request: {:?}",
                                v
                            );
                            None
                        }
                    }
                })
                .or_else(|| req.uri.authority_part().map(|v| v.clone()))
                .ok_or(()),
            return basic_response()
                .status(StatusCode::BAD_REQUEST)
                .body(Body::from("Missing or invalid host headers"))
                .unwrap()
        );
        let base_path: String = req
            .headers
            .get("x-forwarded-prefix")
            .and_then(|v| match v.to_str() {
                Ok(v) => Some(v),
                Err(e) => {
                    debug!("Invalid x-forwarded-host in request: {}", e);
                    None
                }
            })
            .unwrap_or("")
            .into();
        let issuer = join(&proto, &host, &base_path, None);
        let full_url = join(
            &proto,
            &host,
            &base_path,
            Some(
                req.uri
                    .path_and_query()
                    .map(|p| p.as_str())
                    .unwrap_or_else(|| ""),
            ),
        );
        let context = ReqContext {
            proto,
            host,
            base_path,
            issuer,
            full_url,
        };
        match self
            .0
            .routes
            .search(req.uri.path())
            .and_then(|(prefix, _suffix, (exact, proc))| {
                if *exact && prefix.len() != req.uri.path().len() {
                    return None;
                }
                Some(proc)
            }) {
            Some(proc) => proc(&self.0, context, req, body_bytes),
            None => basic_response()
                .status(StatusCode::NOT_FOUND)
                .body(Body::empty())
                .unwrap(),
        }
    }
}

impl NewService for Server {
    type ReqBody = Body;
    type ResBody = Body;
    type Error = hyper::Error;
    type Service = Self;
    type Future = Box<dyn Future<Item = Self::Service, Error = Self::InitError> + Send>;
    type InitError = hyper::Error;

    fn new_service(&self) -> Self::Future {
        Box::new(future::ok(Server(self.0.clone())))
    }
}

impl Service for Server {
    type ReqBody = Body;
    type ResBody = Body;
    type Error = hyper::Error;
    type Future = HttpFut;

    fn call(&mut self, req: Request<Self::ReqBody>) -> Self::Future {
        trace!(
            "Received request {}: {}\nHeaders:\n{}",
            req.method(),
            req.uri(),
            req.headers()
                .iter()
                .map(|h| {
                    let (k, v) = h.borrow();
                    format!("  {}: {}", repr(k.as_ref()), repr(v.as_ref()))
                })
                .collect::<Vec<String>>()
                .join("\n")
        );
        let self1 = self.0.clone();
        let (req, body) = req.into_parts();
        Box::new(
            body.concat2().map(move |body_bytes| -> Response<Body> {
                Server(self1).process(req, body_bytes)
            }),
        )
    }
}

fn submain() -> Result<(), String> {
    let config = Configuration::load(
        &std::env::args()
            .nth(1)
            .ok_or_else(|| "First argument must be config path".to_string())?,
    )?;

    let mut seen_identity: HashSet<&String> = HashSet::new();
    for identity in config.identities.iter() {
        if seen_identity.contains(&identity.name) {
            return Err(format!("Multiple identities have name [{}]", identity.name));
        }
        seen_identity.insert(&identity.name);
    }
    let min_sign_key = max(
        sha2::Sha256::output_size(),
        sodiumoxide::crypto::sign::SEEDBYTES,
    );
    if config.general_key.len() < min_sign_key {
        return Err(format!(
            "Sign key too short, must be at least {} bytes decoded",
            min_sign_key
        ));
    }

    let addr = config
        .listen
        .as_ref()
        .map(|v| {
            SocketAddr::from_str(&v)
                .map_err(|e| format!("Unable to parse listen argument [{}]: {}", v, e))
        })
        .unwrap_or_else(|| Ok(SocketAddr::from_str(DEFAULT_LISTEN.into()).unwrap()))?;
    let addr_ldap = config
        .listen_ldap
        .as_ref()
        .map(|v| {
            SocketAddr::from_str(&v)
                .map_err(|e| format!("Unable to parse listen_ldap argument [{}]: {}", v, e))
        })
        .unwrap_or_else(|| Ok(SocketAddr::from_str(DEFAULT_LISTEN_LDAP.into()).unwrap()))?;

    let mut runtime = runtime::current_thread::Builder::new()
        .clock(Clock::system())
        .build()
        .map_err(|e| format!("Failed to initialize executor: {}", e))?;

    let server = Server::new(&config)?;

    debug!("Starting server on {}", addr);
    runtime.spawn(
        HyperServer::bind(&addr)
            .serve(server.clone())
            .map_err(|e| error!("Uncaught error in server loop: {}", e)),
    );

    debug!("Starting LDAP server on {}", addr_ldap);
    let handle = runtime.handle();
    runtime.spawn(
        TcpListener::bind(&addr_ldap)
            .map_err(|e| format!("Failed to bind to ldap address[{}]: {}", addr_ldap, e))?
            .incoming()
            .map_err(|e| eprintln!("Uncaught error in ldap tcp loop: {}", e))
            .for_each(move |socket| {
                ldap::process(server.clone(), handle.clone(), socket);
                Ok(())
            }),
    );

    runtime.run().map_err(|e| format!("{}", e))?;
    debug!("Done!");

    Ok(())
}

fn main() {
    SimpleLogger::init(
        LevelFilter::Trace,
        ConfigBuilder::new()
            .add_filter_allow("oidle".into())
            .set_time_to_local(true)
            .build(),
    )
    .unwrap();

    match submain() {
        Ok(..) => (),
        Err(e) => error!("Error during setup: {}", e),
    }
}
