use std::process::Command;

fn main() {
    print!("Starting oidle.");
    let mut oidle = Command::new("./oidle")
        .args(&["config.json"])
        .spawn()
        .unwrap();
    print!("Templating nginx.conf.");
    std::fs::write(
        "nginx.conf",
        std::fs::read_to_string("nginx.conf")
            .unwrap()
            .replace("{{PORT}}", &std::env::var("PORT").unwrap_or("80".into())),
    )
    .unwrap();
    print!("Starting nginx.");
    let mut nginx = Command::new("nginx")
        .args(&["-c", "/work/nginx.conf"])
        .spawn()
        .unwrap();
    oidle.wait().unwrap();
    nginx.wait().unwrap();
}
