extern crate handlebars;
extern crate serde;
extern crate simplelog;
extern crate structopt;
extern crate tempfile;

use common::Configuration;
use handlebars::Handlebars;
use log::{debug, warn, LevelFilter};
use serde::Serialize;
use simplelog::{ConfigBuilder, TermLogger, TerminalMode};
use std::process::Command;
use structopt::StructOpt;

fn r(args: &[&str]) {
    debug!("Running {:?}", &args);
    let result = Command::new(args[0]).args(&args[1..]).status().unwrap();
    if !result.success() {
        warn!("Command failed with exit code [{}]: {:?}", result, &args);
    }
}

fn rc(args: &[&str]) {
    debug!("Running (checked) {:?}", &args);
    let result = Command::new(args[0]).args(&args[1..]).status().unwrap();
    if !result.success() {
        panic!("Command failed with exit code [{}]: {:?}", result, &args);
    }
}

#[derive(StructOpt)]
enum GoogleCloudRegion {
    AsiaNortheast1,
    EuropeWest1,
    UsCentral1,
    UsEast1,
}

#[derive(StructOpt)]
struct GoogleCloudSettings {
    #[structopt(subcommand)]
    region: GoogleCloudRegion,
}

#[derive(StructOpt)]
enum Host {
    Gc(GoogleCloudSettings),
}

#[derive(StructOpt)]
#[structopt(about)]
struct Args {
    /// Path to oidle config file, must be in the source tree.
    config: String,

    /// Service on which to deploy
    #[structopt(subcommand)]
    host: Host,

    /// Path to a background image to use instead of the default.  Must end in .jpg
    #[structopt(short, long)]
    background: Option<String>,
}

fn main() {
    TermLogger::init(
        LevelFilter::Trace,
        ConfigBuilder::new()
            .add_filter_allow("oidle".into())
            .set_time_to_local(true)
            .build(),
        TerminalMode::Mixed,
    )
    .unwrap();

    let args: Args = Args::from_args();
    let config = Configuration::load(&args.config).unwrap();

    if config.listen.is_some() {
        panic!("Don't set [listen] in your config when deploying to the cloud.");
    }

    match args.host {
        Host::Gc(gc_settings) => {
            let tempdir = tempfile::tempdir().unwrap();

            std::fs::copy(&args.config, tempdir.path().join("config.json")).unwrap();
            for background in args.background.iter() {
                std::fs::copy(&background, tempdir.path().join("background.jpg")).unwrap();
            }

            let mut handlebars = Handlebars::new();
            handlebars
                .register_template_file("dockerfile", "oidledeploy/Dockerfile")
                .unwrap();
            #[derive(Serialize)]
            struct DockerArgs {
                background: bool,
            }
            std::fs::write(
                tempdir.path().join("Dockerfile"),
                handlebars
                    .render(
                        "dockerfile",
                        &DockerArgs {
                            background: args.background.is_some(),
                        },
                    )
                    .unwrap(),
            )
            .unwrap();

            let region = match gc_settings.region {
                GoogleCloudRegion::AsiaNortheast1 => "asia-northeast1",
                GoogleCloudRegion::EuropeWest1 => "europe-west1",
                GoogleCloudRegion::UsCentral1 => "us-central1",
                GoogleCloudRegion::UsEast1 => "us-east1",
            };
            let project = "oidle--".to_string() + &config.domain.replace("-", "").replace(".", "-");
            let image = format!("gcr.io/{}/oidle", project);
            let service = "oidle";
            r(&["gcloud", "alpha", "projects", "create", &project]);
            r(&[
                "gcloud",
                "domains",
                "verify",
                "--project",
                &project,
                &config.domain,
            ]);
            rc(&[
                "gcloud",
                "builds",
                "submit",
                tempdir.as_ref().to_str().unwrap(),
                "--project",
                &project,
                "--tag",
                &image,
            ]);
            rc(&[
                "gcloud",
                "beta",
                "run",
                "deploy",
                "--region",
                region,
                "--project",
                &project,
                &service,
                "--image",
                &image,
                "--platform",
                "managed",
                "--allow-unauthenticated",
                "--max-instances",
                "1",
                "--memory",
                "256Mi",
            ]);
            r(&[
                "gcloud",
                "beta",
                "run",
                "domain-mappings",
                "create",
                "--region",
                region,
                "--project",
                &project,
                "--service",
                &service,
                "--domain",
                &config.domain,
                "--platform",
                "managed",
            ]);
        }
    }
}
