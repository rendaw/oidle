from ubuntu:18.04
run \
    apt update && \
    apt install -y software-properties-common && \
    apt-add-repository ppa:yubico/stable && \
    apt update && \
    apt install -y nginx libssl1.1 libcbor-dev wget cmake libudev-dev unzip libssl-dev pkg-config && \
    wget https://github.com/Yubico/libfido2/archive/ca27217e1e54fb2de56a09b46d43a247483e6c11.zip -O libfido2.zip && \
    unzip libfido2.zip && cd libfido2-* && mkdir build && cd build && cmake .. && cd .. && make -C build install && \
    rm -rf libfido2* && apt --purge -y autoremove wget cmake unzip pkg-config && \
    true
workdir work
add target/release/oidle .
add target/release/procs .
copy templates templates/
copy static static/
add oidledeploy/nginx.conf .
cmd ./procs
