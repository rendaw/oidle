<img src="https://gitlab.com/rendaw/oidle/raw/master/banner.png" width="100%" alt="oidle - personal identity provider">

<br/>

Oidle is an identity provider.  An identity provider is a server that tells websites who you are.  It's like when you go to Instagram and there's the button "Sign up with Facebook".  When you press that button, Instagram says "Who is this", Facebook replies "This is so-and-so", and Instagram thinks "Oh, I have an account for so-and-so at Facebook" and marks you as logged in.

<br/>

Oidle currently supports!

- **Open ID Connect**
- **Open ID 2.0**
- **LDAP (direct binding)**

You can use Oidle all over the web!  Here's just a few of the two most popular websites Oidle can be used with:

* [OpenStreetMap](https://www.openstreetmap.org/)
* [Gnu Social](https://gnu.io/social/) [(instances)](https://duckduckgo.com/?q=%22it+runs+on+gnu+social%22)

Tested self-hosted software:

* Gitea (OpenID2, LDAP)
* evry/docker-oidc-proxy (OpenID Connect)
* kanboard (LDAP)

Oidle is database-free, single-user, and (mostly) stateless - which makes it easy to manage and scale if you happen to be a group-mind poweruserlifeform.

Oidle supports authentication via password and WebAuthn, so use your Yubikey or Trezor or Finger-vacuum or whatever.

## Warnings!

1. Accounts created with Oidle will be tied to your domain name

   This means if you change your domain name or lose control of it, you'll no longer be able to access your accounts.

2. Account security is tied to your DNS security

   If someone takes over your domain name they can log into your accounts that were managed by Oidle.

3. Accounts created are tied to the identity subject settings

   If you change the `subject` setting or `pairwise_identity` you will no longer be able to access your accounts.

## Generating your configuration

Install [libfido2](https://github.com/Yubico/libfido2), `rust` and `cargo`.

You'll also need a domain name (ex: example.org) and a user name (ex: me).

Create a configuration file with:
```
git clone https://gitlab.com/rendaw/oidle.git
cd oidle
cargo run -p oidlegenerate -p example.org me > config.json
```

See `oidlegenerate -h` for more configuration options.

You should edit the config to define the identity details you want , but otherwise the generated config can be used verbatim.

**Deploying: Google Cloud**

You can host this in Google Cloud Run as a serverless solution - note that when the instance goes cold your sessions will be invalidated so you may have to log in more frequently (depending on where you're logging in).

You'll need to install google-cloud-sdk for the `gcloud` command.

Run:
```
cargo run -p oidledeploy config.json gc us-central1
```
and follow the displayed steps.

**Deploying: Bare metal**

Build Oidle with:

```
cargo build --release -p oidle
```

And start Oidle with
```
./target/release/oidle config.json
```

OpenID connect needs the site to be hosted with SSL so to actually use your instance you'll also need to:

1. Get an [SSL certificate](https://letsencrypt.org/)
2. Set up nginx (or similar) to add SSL to the site and to serve the files in `static/`
3. Configure DNS records for your domain to point to your server

## Configuration notes

**Multiple identities**

If you set up multiple identities, you can choose a identity when logging in to a site.  This can allow you to control how much (and what) personal information each site can access, separate identities, etc.

**Allowed identity claims**

The standardized fields in `claims` are listed [here](https://openid.net/specs/openid-connect-core-1_0.html#StandardClaims), but you can add whatever additional nonstandard claims you'd like.

**Unique claims**

If you have, for example, an email host that supports wildcard email addresses you can use a unique email address with each site by including `{}` in the claim value: for example `{}@example.org` will become something like `q9kh6stdthkadb@example.org`, where the `{}` is replaced with the unique subject id.

**Multiple WebAuthn authenticators**

You can register multiple authenticators, and any one of them can be used to authenticate.  This can be useful if you, for example, want to use your fingerprint on a phone and some other hardware authenticator on your computer.  There's no method for getting authenticator ids outside of `oidlegenerate` at the moment but this may be a future addition.

**Confirming login to sites**

By default, Oidle shows an extra screen to confirm you wish to log into a given website (`"confirm": true`).  If you disable this option and have only one profile, once you've authenticated once you will automatically be logged in at other sites with no further interaction for the duration of the session.

**LDAP Attributes**

Some LDAP connections will check attributes to get user information, both human-readable things such as name and location but also sometimes SSH keys, configuration options, etc.  You can specify arbitrary attributes as string key value-array pairs in the top level `ldap_attributes` field.

# Integration notes

**Gitea - LDAP**

Add a new authentication method with the LDAP - Simple type.

Set `User DN` to just `%s`.  Set `User Filter` to whatever you want, but it can simply be `(%s)` since it's ignored.  Everything else can be left blank, other than the attributes which you should align with the keys you've chosen in the `ldap_attributes` setting in the Oidle config.

**evry/docker-oidc-proxy**

Since docker-oidc-proxy doesn't support dynamic authentication, you'll need to preregister a client id/secret in `clients` in the Oidle config.

Start docker-oidc-proxy with environment variable `OID_DISCOVERY=http://your-host/.well-known/openid-configuration` (that is, specify the path including `.well-known...` rather than just the domain root), setting the other environment variables appropriately.

You may need to set `OID_SESSION_NAME=evry_oidc` (or similar) as well, although I don't believe that has anything to do with Oidle specifically.

**kanboard**

Use settings like:

```php
define('LDAP_AUTH', true);
define('LDAP_BIND_TYPE', 'user');
define('LDAP_USERNAME', '%s');
define('LDAP_USER_BASE_DN', '.');
define('LDAP_USER_FILTER', '(x=%s)');
define('LDAP_USER_ATTRIBUTE_USERNAME', 'username');
define('LDAP_USER_ATTRIBUTE_FULLNAME', 'fullname');
define('LDAP_USER_ATTRIBUTE_EMAIL', 'email');
```

(`BASE_DN` and `USER_FILTER` are ignored on Oidle's side.)

Also set `LDAP_SERVER` and `LDAP_PORT` as required.

# Implementation notes, deviations

* OIDC Authentication endpoint `POST` is not supported, nor are JWT request parameters.
* OIDC Offline access and refresh tokens are not currently supported.
* In OIDC, All profile claims are sent, regardless of requested claims or scopes.
* In OIDC clients using preregistered credentials don't have redirect_uri checked

# Various product recommendations

* Yubikey hardware authenticator for WebAuthn.
* Trezor hardware authenticator for WebAuthn and as a password manager.  The Trezor is seedable so you can also prepare backup devices.
* Gandi.net for domain registration and DNS hosting - they're reputable and support WebAuthn as a login 2FA.
* Fastmail.net for email hosting - they support limited wildcard email addresses.  You need your own domain for wildcard emails.
* Migadu.com for email hosting - they support I think more advanced regular expression addresses, but I haven't tried them.  You need your own domain.

None of these companies have paid me (yet, as of time of writing).
