const ord_dash = '-'.charCodeAt(0);
const ord_underscore = '_'.charCodeAt(0);
const first_lc = 'a'.charCodeAt(0);
const last_lc = 'z'.charCodeAt(0);
const first_uc = 'A'.charCodeAt(0);
const last_uc = 'Z'.charCodeAt(0);
const first_digit = '0'.charCodeAt(0);
const last_digit = '9'.charCodeAt(0);

const urlsafe_b64d = (source) => {
    if (/[^A-Za-z0-9_-]/.test(source)) {
        throw "Invalid base64 data!";
    }

    function codeToU6(ord) {
        return ord >= first_uc && ord <= last_uc ?
            ord - first_uc
            : ord >= first_lc && ord <= last_lc ?
                ord - 71
                : ord >= first_digit && ord <= last_digit ?
                    ord + 4
                    : ord === ord_dash ?
                        62
                        : ord === ord_underscore ?
                            63
                            :
                            0;
    }

    const outLen = source.length * 3 + 1 >>> 2;
    const out = new Uint8Array(outLen);

    for (var nMod3, nMod4, nUint24 = 0, nOutIdx = 0, nInIdx = 0; nInIdx < source.length; nInIdx++) {
        nMod4 = nInIdx & 3;
        nUint24 |= codeToU6(source.charCodeAt(nInIdx)) << 18 - 6 * nMod4;
        if (nMod4 === 3 || source.length - nInIdx === 1) {
            for (nMod3 = 0; nMod3 < 3 && nOutIdx < outLen; nMod3++ , nOutIdx++) {
                out[nOutIdx] = nUint24 >>> (16 >>> nMod3 & 24) & 255;
            }
            nUint24 = 0;
        }
    }

    return out;
}

const urlsafe_b64e = (source) => {
    function u6ToCode(val) {
        return val < 26 ?
            val + 65
            : val < 52 ?
                val + 71
                : val < 62 ?
                    val - 4
                    : val === 62 ?
                        ord_dash
                        : val === 63 ?
                            ord_underscore
                            :
                            65;
    }


    let outLen = (3 - (source.length % 3)) % 3;
    let out = "";
    const sourceLen = source.length;

    let nUint24 = 0;
    for (let i = 0; i < sourceLen; i++) {
        let nMod3 = i % 3;
        nUint24 |= source[i] << (16 >>> nMod3 & 24);
        if (nMod3 === 2 || source.length - i === 1) {
            out += String.fromCharCode(u6ToCode(nUint24 >>> 18 & 63), u6ToCode(nUint24 >>> 12 & 63), u6ToCode(nUint24 >>> 6 & 63), u6ToCode(nUint24 & 63));
            nUint24 = 0;
        }
    }

    return outLen === 0 ?
        out
        :
        out.substring(0, out.length - outLen);
}

const decode_entities = text => {
    let temp = document.createElement("textarea");
    temp.innerHTML = text;
    return temp.value;
};

const data = JSON.parse(decode_entities(document.getElementById('data').textContent));

const root = document.documentElement;
navigator.credentials.get({
    publicKey: {
        challenge: urlsafe_b64d(data.challenge),
        allowCredentials: data.ids.map(id => {
            return {
                id: urlsafe_b64d(id),
                type: "public-key",
            }
        }),
        rpId: data.domain,
    }
}).then(result => {
    document.getElementsByName("authenticatorId")[0].setAttribute("value", result.id)
    document.getElementsByName("clientDataJSON")[0].setAttribute("value", urlsafe_b64e(new Uint8Array(result.response.clientDataJSON)))
    document.getElementsByName("signature")[0].setAttribute("value", urlsafe_b64e(new Uint8Array(result.response.signature)))
    document.getElementsByName("authenticatorData")[0].setAttribute("value", urlsafe_b64e(new Uint8Array(result.response.authenticatorData)))
    document.getElementById("form").submit()
}).catch(reason => {
    document.getElementById("error").textContent = "Error - check your console for details."
    console.error(reason.name, reason.message, "\n", reason);
})

