const decode_entities = text => {
    let temp = document.createElement("textarea");
    temp.innerHTML = text;
    return temp.value;
};

const identities = JSON.parse(decode_entities(document.getElementById('data').textContent));

const select = document.getElementsByName('identity')[0];
const defs_title = document.querySelector("#identity > h1");
const defs = document.querySelector('#identity > dl');

const select_identity = key => {
    const identity = identities.find(p => p[0] === key);

    defs_title.textContent = "Identity - " + identity[1]["name"];

    defs.innerHTML = "";
    const claims = identity[1]["claims"];
    Object.keys(claims).forEach(claims_key => {
        if (claims_key === "address") {
            const address = claims[claims_key];
            Object.keys(address).forEach(address_key => {
                const dd = document.createElement("dd");
                dd.textContent = address_key;
                defs.appendChild(dd);
                const dt = document.createElement("dt");
                dt.textContent = address[address_key];
                defs.appendChild(dt);
            });
        } else {
            const dd = document.createElement("dd");
            dd.textContent = claims_key;
            defs.appendChild(dd);
            const dt = document.createElement("dt");
            dt.textContent = claims[claims_key];
            defs.appendChild(dt);
        }
    })
};

select.addEventListener("change", e => {
    select_identity(e.target.value);
});

identities.forEach(identity => {
    const option = document.createElement("option");
    option.setAttribute("value", identity[0]);
    option.textContent = identity[1]["name"];
    select.appendChild(option);
});

select_identity(identities[0][0]);