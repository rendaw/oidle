pub extern crate base64;
pub extern crate base64_serde;
pub extern crate rand;
pub extern crate serde as serde_;
pub extern crate serde_json;
pub extern crate serde_with_macros;
pub extern crate sha2;
use crate::serde::{Deserialize, Deserializer, Serialize, Serializer};
use base64::{decode_config as b64d_, encode_config as b64e_, URL_SAFE_NO_PAD};
use base64_serde::base64_serde_type;
use rand::Rng;
use serde_json::Value;
use sha2::{Digest, Sha256};
use std::collections::HashMap;
use std::convert::TryInto;
use std::fmt::{Debug, Formatter};
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

pub mod bindings_fido2;
pub mod fido2;

pub mod serde {
    pub use serde_::*;
}

//
// Serialization/deserialization tools
pub fn sha256(s: &[u8]) -> [u8; 32] {
    let mut hash = Sha256::default();
    hash.input(s);
    hash.result().as_slice().try_into().unwrap()
}

pub fn rand_str<R: Rng>(r: &mut R, count: usize) -> String {
    let raw = (0..count)
        .map(|_| {
            let v = r.gen_range(0, 26 + 10);
            if v < 26 {
                b'a' + v
            } else {
                b'0' + v
            }
        })
        .collect::<Vec<u8>>();
    unsafe { std::str::from_utf8_unchecked(raw.as_slice()) }.to_string()
}

pub fn b64e<T: ?Sized + AsRef<[u8]>>(t: &T) -> String {
    b64e_(t, URL_SAFE_NO_PAD)
}

pub fn b64d<T: ?Sized + AsRef<[u8]>>(input: &T) -> Result<Vec<u8>, base64::DecodeError> {
    b64d_(input, URL_SAFE_NO_PAD)
}

base64_serde_type!(SerdeB64, URL_SAFE_NO_PAD);

pub struct ConfigurationBytes(pub Vec<u8>);

impl Serialize for ConfigurationBytes {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        SerdeB64::serialize(&self.0, serializer)
    }
}

impl<'de> Deserialize<'de> for ConfigurationBytes {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        SerdeB64::deserialize(deserializer).map(|v| ConfigurationBytes(v))
    }
}

impl Debug for ConfigurationBytes {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        self.0.fmt(f)
    }
}

impl From<&[u8]> for ConfigurationBytes {
    fn from(v: &[u8]) -> Self {
        ConfigurationBytes(v.to_vec())
    }
}

impl From<Vec<u8>> for ConfigurationBytes {
    fn from(v: Vec<u8>) -> Self {
        ConfigurationBytes(v)
    }
}

//
// Misc
#[macro_export]
macro_rules! debug_display {
    ($t:ident) => {
        impl std::fmt::Display for $t {
            fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
                std::fmt::Debug::fmt(self, f)
            }
        }
    };
}

//
// Constants

pub const DEFAULT_LISTEN: &str = "0.0.0.0:2342";
pub const DEFAULT_LISTEN_LDAP: &str = "0.0.0.0:1389";
pub const DEFAULT_AUTHENTICATION_TOKEN_EXPIRY: i64 = 180;
pub const DEFAULT_ACCESS_TOKEN_EXPIRY: i64 = 180;
pub const DEFAULT_ID_TOKEN_VERIFICATION_EXPIRY: i64 = 180;
pub const DEFAULT_SESSION_INCOMPLETE_EXPIRY: i64 = 60 * 5; // 5m
pub const DEFAULT_SESSION_VALID_EXPIRY: i64 = 60 * 60 * 24 * 7; // 1wk

//
// Serialization types

#[derive(Debug, Serialize, Deserialize)]
pub struct Configuration {
    /// User name for _authentication_
    pub username: String,
    /// Domain, including subdomain, oidle will be hosted on
    pub domain: String,
    #[serde(with = "SerdeB64")]
    pub jwt_pubpriv_key_der: Vec<u8>,
    #[serde(with = "SerdeB64")]
    pub general_key: Vec<u8>,
    #[serde(with = "SerdeB64")]
    pub general_key_mask: Vec<u8>,
    /// Only disable this for local testing
    pub force_https: bool,
    /// Password for _authentication_
    pub password: Option<ConfigurationBytes>,
    pub webauthn: Vec<(String, String)>,
    /// If true, after authentication oidle will ask you to confirm your login.  Set to false to allow one-click (zero-click?) login to sites once you've establisthed an oidle session.
    pub confirm: bool,
    /// Host and port for the server to listen to for openid connect requests
    pub listen: Option<String>,
    /// How long before the authentication token expires in seconds (this is a temporary token, shouldn't need to be long)
    pub authentication_token_expiry: Option<i64>,
    /// How long before the access token expires in seconds (this is a temporary token, shouldn't need to be long)
    pub access_token_expiry: Option<i64>,
    /// How long before the identity token expires in seconds.  This corresponds to the session length on the site you're logging into.
    pub id_token_verification_expiry: Option<i64>,
    /// All the different ways you can present yourself to different sites you're logging into
    pub identities: Vec<ConfigurationIdentity>,
    /// How long before your oidle session expires if you don't complete the authentication
    pub session_incomplete_expiry: Option<i64>,
    /// How long before your oidle session expires once you've completed authentication
    pub session_valid_expiry: Option<i64>,
    /// Preregistered client ids and secrets, for clients that don't support dynamic registration.  For OIDC, these can have any pattern.
    pub clients: HashMap<String, String>,
    /// Host and port for the server to listen to for ldap connections
    pub listen_ldap: Option<String>,
    /// Arbitrary string key/value pairs to include with the LDAP entity search result
    pub ldap_attributes: HashMap<String, Vec<String>>,
}

impl Configuration {
    pub fn load(path: &str) -> Result<Configuration, String> {
        let file = File::open(Path::new(path))
            .map_err(|e| format!("Error opening config file [{}]: {}", path, e))?;
        let reader = BufReader::new(file);
        Ok(serde_json::from_reader(reader)
            .map_err(|e| format!("Error reading config [{}]: {}", path, e))?)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ConfigurationIdentity {
    /// A way for you to distinguish identities
    pub name: String,
    /// If true, subject will be scrambled uniquely for each site you log into
    pub pairwise_subject: bool,
    /// A unique id presented to sites you log into - included in `claims` as well as other protocol messages
    pub sub: String,
    /// Arbitrary data clients can request.  In OpenID Connect the keys are from the list [here](https://openid.net/specs/openid-connect-core-1_0.html#StandardClaims), although you may include anything you'd like.  In OpenID 2 the keys are [here](https://openid.net/specs/openid-attribute-properties-list-1_0-01.html) (the full URIs should be the key).
    pub claims: HashMap<String, Value>,
}
