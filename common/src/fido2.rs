use crate::bindings_fido2;
use crate::bindings_fido2::{
    fido_assert_free, fido_assert_new, fido_assert_set_authdata_raw,
    fido_assert_set_clientdata_hash, fido_assert_set_count, fido_assert_set_rp,
    fido_assert_set_sig, fido_assert_t, fido_assert_verify, fido_cred_free, fido_cred_id_len,
    fido_cred_id_ptr, fido_cred_new, fido_cred_pubkey_len, fido_cred_pubkey_ptr,
    fido_cred_set_clientdata_hash, fido_cred_set_rp, fido_cred_set_type, fido_dev_close,
    fido_dev_free, fido_dev_info_free, fido_dev_info_manifest, fido_dev_info_new,
    fido_dev_info_path, fido_dev_info_ptr, fido_dev_info_t, fido_dev_make_cred, fido_dev_new,
    fido_dev_open, fido_init as fido_init_, fido_strerr, FIDO_DEBUG, FIDO_OK,
};
use crate::fido2::Fido2Error::NewFailed;
use core::fmt;
use std::ffi::{CStr, CString};
use std::fmt::{Error, Formatter};
use std::os::raw;
use std::os::raw::{c_char, c_int, c_void};
use std::ptr::{null, null_mut};
use std::slice::from_raw_parts;

#[derive(Debug, Clone)]
pub enum Fido2Error {
    Raw(c_int),
    NewFailed,
}

impl std::error::Error for Fido2Error {}

impl fmt::Display for Fido2Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        f.write_str(match self {
            Fido2Error::Raw(e) => unsafe {
                CStr::from_ptr(fido_strerr(*e))
                    .to_str()
                    .map_err(|_| Error)?
            },
            NewFailed => "Instantiation failed",
        })
    }
}

type Fido2Result<T> = Result<T, Fido2Error>;

// Aux
fn check_null<T, F: FnOnce() -> *mut T>(f: F) -> Fido2Result<*mut T> {
    let out = f();
    if out == null_mut() {
        return Err(NewFailed);
    }
    Ok(out)
}

fn check_ok<F: FnOnce() -> raw::c_int>(f: F) -> Fido2Result<()> {
    let res = f();
    if res == FIDO_OK as raw::c_int {
        return Ok(());
    }
    Err(Fido2Error::Raw(res))
}

// Wrappers
pub fn fido_init() {
    unsafe {
        fido_init_(
            std::env::var("FIDO_DEBUG")
                .ok()
                .and_then(|v| match v.as_str() {
                    "1" => Some(FIDO_DEBUG),
                    _ => None,
                })
                .unwrap_or(0) as i32,
        );
    }
}

pub struct Fido2DevListInner {
    raw: *mut fido_dev_info_t,
    alloc_n: usize,
}

impl Drop for Fido2DevListInner {
    fn drop(&mut self) {
        unsafe {
            fido_dev_info_free(&mut self.raw, self.alloc_n);
        }
    }
}

pub struct Fido2DevList {
    inner: Fido2DevListInner,
    count: usize,
}

impl Fido2DevList {
    pub fn new(n: usize) -> Fido2Result<Fido2DevList> {
        let inner = Fido2DevListInner {
            raw: check_null(|| unsafe { fido_dev_info_new(n) })?,
            alloc_n: n,
        };
        let mut count: usize = 0;
        check_ok(|| unsafe { fido_dev_info_manifest(inner.raw, n, &mut count) })?;
        Ok(Fido2DevList { inner, count })
    }

    pub fn get_path(&self, i: usize) -> Fido2Result<std::ffi::CString> {
        assert!(i < self.count);
        unsafe {
            Ok(CStr::from_ptr(fido_dev_info_path(fido_dev_info_ptr(self.inner.raw, i))).to_owned())
        }
    }
}

pub struct Fido2DevListIter {
    parent: Fido2DevList,
    i: usize,
}

impl Iterator for Fido2DevListIter {
    type Item = CString;

    fn next(&mut self) -> Option<Self::Item> {
        if self.i < self.parent.count {
            let i = self.i;
            self.i += 1;
            Some(self.parent.get_path(i).unwrap())
        } else {
            None
        }
    }
}

impl IntoIterator for Fido2DevList {
    type Item = std::ffi::CString;
    type IntoIter = Fido2DevListIter;

    fn into_iter(self) -> Self::IntoIter {
        Fido2DevListIter { parent: self, i: 0 }
    }
}

struct Fido2DevInner {
    raw: *mut bindings_fido2::fido_dev_t,
}

impl Drop for Fido2DevInner {
    fn drop(&mut self) {
        unsafe {
            fido_dev_free(&mut self.raw);
        }
    }
}

pub struct Fido2Dev {
    inner: Fido2DevInner,
}

impl Fido2Dev {
    pub fn new(path: &CStr) -> Fido2Result<Fido2Dev> {
        let inner = Fido2DevInner {
            raw: check_null(|| unsafe { fido_dev_new() })?,
        };
        check_ok(|| unsafe { fido_dev_open(inner.raw, path.as_ptr()) })?;
        Ok(Fido2Dev { inner })
    }

    pub fn make_cred(&self, cred: &Fido2Cred, pin: Option<&[u8]>) -> Fido2Result<()> {
        check_ok(|| unsafe {
            fido_dev_make_cred(
                self.inner.raw,
                cred.raw,
                pin.map(|v| v.as_ptr()).unwrap_or(null()) as *const c_char,
            )
        })
    }
}

impl Drop for Fido2Dev {
    fn drop(&mut self) {
        unsafe {
            fido_dev_close(self.inner.raw);
        }
    }
}

pub struct Fido2Cred {
    raw: *mut bindings_fido2::fido_cred_t,
}

impl Fido2Cred {
    pub fn new() -> Fido2Result<Fido2Cred> {
        Ok(Fido2Cred {
            raw: check_null(|| unsafe { fido_cred_new() })?,
        })
    }

    pub fn set_type(&self, t: i32) -> Fido2Result<()> {
        check_ok(|| unsafe { fido_cred_set_type(self.raw, t) })
    }

    pub fn set_clientdata_hash(&self, cdh: &[u8]) -> Fido2Result<()> {
        check_ok(|| unsafe { fido_cred_set_clientdata_hash(self.raw, cdh.as_ptr(), cdh.len()) })
    }

    pub fn set_rp(&self, id: &str, name: &str) -> Fido2Result<()> {
        let c_id = CString::new(id).unwrap();
        let c_name = CString::new(name).unwrap();
        check_ok(|| unsafe {
            fido_cred_set_rp(
                self.raw,
                c_id.as_ptr(),
                c_name.as_ptr(),
            )
        })
    }

    pub fn pubkey(&self) -> Fido2Result<Option<Vec<u8>>> {
        unsafe {
            let temp = fido_cred_pubkey_ptr(self.raw);
            if temp == null() {
                return Ok(None);
            }
            Ok(Some(
                from_raw_parts(temp, fido_cred_pubkey_len(self.raw)).into(),
            ))
        }
    }

    pub fn id(&self) -> Fido2Result<Option<Vec<u8>>> {
        unsafe {
            let temp = fido_cred_id_ptr(self.raw);
            if temp == null() {
                return Ok(None);
            }
            Ok(Some(
                from_raw_parts(temp, fido_cred_id_len(self.raw)).into(),
            ))
        }
    }
}

impl Drop for Fido2Cred {
    fn drop(&mut self) {
        unsafe { fido_cred_free(&mut self.raw) }
    }
}

pub struct Fido2AssertInner {
    raw: *mut fido_assert_t,
}

impl Drop for Fido2AssertInner {
    fn drop(&mut self) {
        unsafe { fido_assert_free(&mut self.raw) }
    }
}

pub struct Fido2Assert {
    inner: Fido2AssertInner,
}

impl Fido2Assert {
    pub fn new() -> Fido2Result<Fido2Assert> {
        let inner = Fido2AssertInner {
            raw: check_null(|| unsafe { fido_assert_new() })?,
        };
        check_ok(|| unsafe { fido_assert_set_count(inner.raw, 1) })?;
        Ok(Fido2Assert { inner })
    }

    pub fn set_clientdata_hash(&self, cdh: &[u8]) -> Fido2Result<()> {
        check_ok(|| unsafe {
            fido_assert_set_clientdata_hash(self.inner.raw, cdh.as_ptr(), cdh.len())
        })
    }

    pub fn set_authdata_raw(&self, authdata: &[u8]) -> Fido2Result<()> {
        check_ok(|| unsafe {
            fido_assert_set_authdata_raw(self.inner.raw, 0, authdata.as_ptr(), authdata.len())
        })
    }

    pub fn set_rp(&self, id: &str) -> Fido2Result<()> {
        let c_id = CString::new(id).unwrap();
        check_ok(|| unsafe {
            fido_assert_set_rp(self.inner.raw, c_id.as_ptr())
        })
    }

    pub fn set_sig(&self, sig: &[u8]) -> Fido2Result<()> {
        check_ok(|| unsafe { fido_assert_set_sig(self.inner.raw, 0, sig.as_ptr(), sig.len()) })
    }

    pub fn verify(&self, t: i32, public_key: &[u8]) -> Fido2Result<()> {
        check_ok(|| unsafe {
            fido_assert_verify(self.inner.raw, 0, t, public_key.as_ptr() as *const c_void)
        })
    }
}
